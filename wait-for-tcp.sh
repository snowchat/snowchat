#!/usr/bin/env sh

HOST=$1
shift
PORT=$1
shift

echo "Waiting for $HOST:$PORT to become available"
echo "Will run the command \"$@\""

if command -v nc &> /dev/null; then
    USE_NC=1
    echo 'Using the nc utility'
else
    USE_NC=0
fi

start_ts=$(date +%s)
while :
do
    if [[ $USE_NC -eq 1 ]]; then
        nc -z $HOST $PORT
        result=$?
    else
        (echo -n > /dev/tcp/$HOST/$PORT) >/dev/null 2>&1
        result=$?
    fi
    if [[ $result -eq 0 ]]; then
        end_ts=$(date +%s)
        echo "$HOST:$PORT is available after $((end_ts - start_ts)) seconds"
        break
    fi
    sleep 1
done

echo 'Starting...'
exec "$@"
