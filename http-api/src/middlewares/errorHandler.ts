import { NextFunction, Request, Response } from 'express';
import { HTTPError } from 'lambert-server';
import { FieldError } from '@snowchat/util';

export function errorHandler(error: Error, req: Request, res: Response, next: NextFunction) {
  if (!error) next();

  try {
    let httpcode = 400;
    let message = error?.toString();
    let errors = undefined;

    if (error instanceof HTTPError && error.code) httpcode = error.code;
    else if (error instanceof FieldError) {
      message = error.message;
      errors = error.errors;
      if (error.statusCode) httpcode = error.statusCode;
    } else {
      console.error(error);
      if (req.server?.options?.production) {
        message = 'Internal Server Error';
      }
      httpcode = 500;
    }

    if (httpcode > 511) httpcode = 400;

    console.error(`[HTTPError] ${req.url} ${message}`, errors || error);

    res.status(httpcode).json({ message, errors });
  } catch (error) {
    console.error(`[Internal Server Error] 500`, error);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
}
