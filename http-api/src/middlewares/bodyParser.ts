import express, { NextFunction, Request, Response } from 'express';
import { HTTPError } from 'lambert-server';
import * as bodyParserLibrary from 'body-parser';

export function bodyParser(opts?: bodyParserLibrary.OptionsJson) {
  const jsonParser = express.json(opts);

  return (req: Request, res: Response, next: NextFunction) => {
    jsonParser(req, res, (err) => {
      if (err) {
        console.log('[BodyParser] Error', err);
        // TODO: different errors: size limit, invalid body, etc.
        return next(new HTTPError('Invalid Body', 400));
      }
      next();
    });
  };
}
