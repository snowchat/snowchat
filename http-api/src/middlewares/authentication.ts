import { NextFunction, Request, Response } from 'express';
import { HTTPError } from 'lambert-server';
import { checkToken } from '@snowchat/util';

export const NO_AUTHORIZATION_ROUTES = [
  /^\/auth\/login\/?$/, // login route
  /^\/auth\/register\/?$/, // register route
  /^\/webhooks/, // webhook route has own token
  /^\/ping\/?$/, // ping route
  /^\/gateway\/?$/, // gateway url get route
  /^\/$/, // home route
];

export const API_PREFIX = /^\/api(\/v\d+)?/;
export const API_PREFIX_TRAILING_SLASH = /^\/api(\/v\d+)?\//;

declare global {
  namespace Express {
    interface Request {
      user_id: any;
      user_bot: boolean;
      token: any;
    }
  }
}

export async function authentication(req: Request, res: Response, next: NextFunction) {
  if (req.method === 'OPTIONS') return res.sendStatus(204);
  const url = req.url.replace(API_PREFIX, '');
  if (
    NO_AUTHORIZATION_ROUTES.some((x) => {
      if (typeof x === 'string') return url.startsWith(x);
      return x.test(url);
    })
  ) {
    console.log(
      'Skipped authentication for route',
      url,
      'because of filter(s)',
      NO_AUTHORIZATION_ROUTES.filter((x) => {
        if (typeof x === 'string') return url.startsWith(x);
        return x.test(url);
      }),
    );
    return next();
  }
  if (!req.headers.authorization) throw new HTTPError('Missing Authorization Header', 401);

  try {
    const { decoded, user } = await checkToken(req.headers.authorization);

    req.token = decoded;
    req.user_id = decoded.id;
    req.user_bot = user.bot;
    return next();
  } catch (error) {
    // @ts-ignore
    throw new HTTPError(error.toString(), 400);
  }
}
