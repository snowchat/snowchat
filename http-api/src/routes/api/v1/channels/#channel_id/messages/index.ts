import { emitEvent, Channel, Message, Snowflake, ChannelType, User, MessageType, FieldErrors } from '@snowchat/util';
import { MessageCreateEvent } from '@snowchat/util/dist/interfaces/messageEvents';
import { Router, Response, Request, NextFunction } from 'express';
import { check, HTTPError, Tuple } from 'lambert-server';
import multer from 'multer';
import { FindManyOptions, LessThan, MoreThan } from 'typeorm';

const router = Router();

const messageUpload = multer({
  limits: {
    fileSize: 1024 * 1024 * 100,
    fields: 10,
    files: 1,
  },
  storage: multer.memoryStorage(),
}); // max upload 50 mb

export type MessageCreateSchema = Partial<Message>;

export function enumToTuple(convertable: any) {
  new Tuple(
    Object.keys(convertable).filter((item) => {
      return isNaN(Number(item));
    }),
  );
}

// TODO: validate body
router.post(
  '/',
  messageUpload.single('file'),
  async (req: Request, res: Response, next: NextFunction) => {
    if (req.body.payload_json) {
      req.body = JSON.parse(req.body.payload_json);
    }
    next();
  },
  check({
    content: String,
    $embeds: [
      {
        title: String,
        url: String,
        image_url: String,
        thumbnail_url: String,
        author: String,
      },
    ],
    $tts: Boolean,
    $pinned: Boolean,
    $type: enumToTuple(MessageType),
  }),
  async (req: Request, res: Response) => {
    const { channel_id } = req.params;
    const body = req.body as MessageCreateSchema;
    if (
      body.type &&
      body.type in
        [
          MessageType.EVENT_REMINDER,
          MessageType.BOOST,
          MessageType.BOOST_TIER_1,
          MessageType.BOOST_TIER_2,
          MessageType.BOOST_TIER_3,
          MessageType.CHANNEL_FOLLOW_ADD,
          MessageType.GUILD_MEMBER_JOIN,
          MessageType.CALL,
        ]
    ) {
      throw FieldErrors({
        type: {
          code: 'CANNOT_SEND_TYPE',
          message: 'Client cannot send this type of message',
        },
      });
    }

    if (req.file) {
      // TODO: Attachments
      throw FieldErrors(
        {
          type: {
            code: 'NOT_IMPLEMENTED',
            message: 'Attachments are not implemented',
          },
        },
        501,
        'NOT_IMPLEMENTED',
      );
    }

    const author = await User.getPublicUser(req.user_id);

    const channel = await Channel.findOne({ where: { id: channel_id } });
    if (!channel)
      throw FieldErrors(
        {
          channel_id: {
            code: 'NOT_FOUND',
            message: 'Channel Not Found',
          },
        },
        404,
      );
    // @ts-ignore
    let message = Message.create([
      {
        id: Snowflake.generate(),
        guild_id: channel.guild_id,
        channel_id: channel_id,
        author_id: req.user_id,
        attachments: [],
        embeds: [],
        reactions: [],
        type: 0,
        edited_timestamp: undefined,
        timestamp: new Date(),
        pinned: false,
        content: body.content || 'Invalid Message',
      },
    ])[0];
    message = await message.save();

    channel.assign({ last_message_id: message.id });
    await channel.save();

    emitEvent({
      event: 'MESSAGE_CREATE',
      channel_id,
      data: {
        ...message,
        author,
      },
    } as MessageCreateEvent);
    console.log('AUTHOR', author);

    return res.json({ ...message, author });
  },
);

router.get('/', async (req: Request, res: Response) => {
  const channel_id = req.params.channel_id;
  const channel = await Channel.findOne({ id: channel_id });
  if (!channel)
    throw FieldErrors(
      {
        channel_id: {
          code: 'NOT_FOUND',
          message: 'Channel Not Found',
        },
      },
      404,
    );

  if (channel.type != ChannelType.TEXT) throw new HTTPError('Invalid Channel Type', 400);
  const around = req.query.around ? `${req.query.around}` : undefined;
  const before = req.query.before ? `${req.query.before}` : undefined;
  const after = req.query.after ? `${req.query.after}` : undefined;
  const limit = Number(req.query.limit) || 50;
  if (limit < 1 || limit > 100) throw new HTTPError('Message limit must be between 1 and 100');

  var halfLimit = Math.floor(limit / 2);

  // TODO: Check if has view channel permission
  // TODO: If no 'read message history' permission return an empty list

  var query: FindManyOptions<Message> & { where: { id?: any } } = {
    order: { id: 'DESC' },
    take: limit,
    where: { channel_id },
    relations: ['author'],
  };

  if (after) query.where.id = MoreThan(after);
  else if (before) query.where.id = LessThan(before);
  else if (around) {
    query.where.id = [
      MoreThan((BigInt(around) - BigInt(halfLimit)).toString()),
      LessThan((BigInt(around) + BigInt(halfLimit)).toString()),
    ];
  }

  const messages = await Message.find(query);

  return res.json(
    messages.map((m) => {
      if (!m.author) {
        // @ts-ignore
        m.author = {
          discriminator: 0,
          username: 'Deleted User',
          avatar_url: undefined,
          banner_url: undefined,
          deleted: true,
          bot: false,
          developer: false,
          bio: 'Deleted User',
        };
      }

      return m;
    }),
  );
});

export default router;
