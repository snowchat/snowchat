import { Router, Response, Request } from 'express';
import { HTTPError } from 'lambert-server';

const router = Router();

router.post('*', (req: Request, res: Response) => {
  throw new HTTPError('Not Implemented', 501);
});

export default router;
