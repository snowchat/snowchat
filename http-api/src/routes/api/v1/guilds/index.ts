import { Router, Response, Request } from 'express';
import { Tuple, check, HTTPError } from 'lambert-server';
import { config, Member, Guild, Snowflake, MessageNotificationLevel, Channel, ChannelTypes } from '@snowchat/util';

const router = Router();

export interface GuildCreateSchema {
  name: string;
  description?: string | null;
  icon_url?: string | null;
  banner_url?: string | null;
  channels?: Partial<Channel>[] | null;
}

export const ChannelCreateSchema = {
  type: new Tuple(...Object.values(ChannelTypes)),
  name: String,
  $parent_id: String,
  $position: Number,
  $description: String,
  $permission_overwrites: [
    {
      allow: BigInt,
      deny: BigInt,
      id: String,
      type: new Tuple(0, 1),
    },
  ],
  $nsfw: Boolean,
  $icon: new Tuple('rules'), // TODO: more icon types
  $bitrate: Number, // TODO: maximum and minimum bitrate
  $user_limit: Number, // TODO: maximum and minimum user limit
  $rate_limit: Number, // TODO: maximum and minimum rate limit
};

export const GuildCreateSchema = {
  name: String,
  $description: String,
  $icon_url: String,
  $banner_url: String,
  $channels: [
    {
      ...ChannelCreateSchema,
      id: Number,
      $parent_id: Number,
    },
  ],
};

// TODO: put schemas in a folder or typescript file
router.post('/', check(GuildCreateSchema), async (req: Request, res: Response) => {
  const body = req.body as GuildCreateSchema;

  console.log('User', req.user_id, 'is creating a guild with the following data:', body);

  const currentConfig = config.get();
  const { maxJoinedGuilds } = currentConfig.limits.user;
  const guild_count = await Member.count({ id: req.user_id });
  if (guild_count >= maxJoinedGuilds) {
    throw new HTTPError(`Maximum number of guilds reached (${maxJoinedGuilds}).`, 400);
  }

  const guild_id = Snowflake.generate();

  await Guild.insert({
    id: guild_id,
    name: body.name,
    description: body.description || '',
    icon_url: body.icon_url || undefined,
    banner_url: body.banner_url || undefined,
    member_count: 0, // member helper function will increase
    default_message_notifications: MessageNotificationLevel.ALL,
    created_at: new Date(),
    owner_id: req.user_id,
    boost_subscription_count: 0,
    boost_tier: 0,
  });

  // TODO: create @everyone role

  if (!body.channels || !body.channels.length) body.channels = [{ id: '01', type: 0, name: 'general' }];

  const ids = new Map();

  body.channels.forEach((channel) => {
    if (channel.id) {
      ids.set(channel.id, Snowflake.generate());
    }
  });

  await Promise.all(
    body.channels?.map((channel) => {
      const id = ids.get(channel.id) || Snowflake.generate();
      const parent_id = ids.get(channel.parent_id);

      return Channel.createChannel({ ...channel, guild_id, id, parent_id }, req.user_id, true, true, false);
    }),
  );

  await Member.addToGuild(req.user_id, guild_id);

  res.status(201).json({ id: guild_id });
});

export default router;
