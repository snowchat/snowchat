import { Router, Response, Request } from 'express';
import { Channel, FieldErrors, Guild, Snowflake } from '@snowchat/util';
import { check } from 'lambert-server';
import { ChannelCreateSchema } from '../index';
const router = Router();

router.post('/', check(ChannelCreateSchema), async (req: Request, res: Response) => {
  const { guild_id } = req.params;
  const guild = await Guild.findOne({ id: guild_id });
  if (!guild)
    throw FieldErrors(
      {
        guild_id: {
          code: 'NOT_FOUND',
          message: 'Guild not found',
        },
      },
      404,
    );
  const channelJson = req.body as Partial<Channel>;

  /*const channel = Channel.create({
    ...channelJson,
    id: Snowflake.generate(),
    guild_id,
  });

  await channel.save();*/

  const channel = await Channel.createChannel({ ...channelJson, guild_id }, req.user_id);

  res.status(201).json(channel);
});

router.get('/', async (req: Request, res: Response) => {
  const { guild_id } = req.params;
  const channels = await Channel.find({ guild_id });

  res.json(channels);
});

export default router;
