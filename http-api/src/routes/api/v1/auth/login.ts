import { Request, Response, Router } from 'express';
import { check } from 'lambert-server';
import * as argon2 from 'argon2';
import jwt from 'jsonwebtoken';
import { config, User, FieldErrors } from '@snowchat/util';
import { correctEmail } from './register';

const router: Router = Router();

router.post(
  '/',
  check({
    email: String,
    password: String,
  }),
  async (req: Request, res: Response) => {
    const { email: rawEmail, password } = req.body;
    const email = correctEmail(rawEmail);
    if (!email) {
      throw FieldErrors({ email: { message: 'Invalid E-Mail', code: 'INVALID_EMAIL' } });
    }

    console.log('[Login] E-Mail:', email);

    // TODO: captcha

    const user = await User.findOne(
      {
        email: email,
      },
      {},
    );
    if (!user) {
      console.log('[E-Mail Not Found]', email);
      throw FieldErrors({ email: { message: 'E-Mail not found', code: 'INVALID_EMAIL' } }, 404);
    }

    try {
      const samePassword = await argon2.verify(user.password_hash || '', password);
      if (!samePassword) {
        console.log('[Invalid Password] Passwords did not match');
        throw FieldErrors({ password: { message: 'Invalid Password', code: 'INVALID_PASSWORD' } });
      }
    } catch (err) {
      console.log('[Invalid Password] Error while verifying password', err);
      throw FieldErrors({ password: { message: 'Invalid Password', code: 'INVALID_PASSWORD' } });
    }

    const token = await generateToken(user.id);

    // This will incorporate a separate token structure than Discord.
    // The Discord token header is only the user id as a Base64 string but it is impossible with the jsonwebtoken library.
    // https://user-images.githubusercontent.com/6506416/81051916-dd8c9900-8ec2-11ea-8794-daf12d6f31f0.png

    res.json({ token });
  },
);

export async function generateToken(id: string) {
  const iat = Math.floor(Date.now() / 1000);
  const algorithm = 'HS256';

  return new Promise((res, rej) => {
    jwt.sign(
      { id: id, iat },
      config.get().security.jwtSecret,
      {
        algorithm,
      },
      (err, token) => {
        if (err) return rej(err);
        return res(token);
      },
    );
  });
}

export default router;
