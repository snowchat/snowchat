import { Router, Response, Request } from 'express';
import { HTTPError } from 'lambert-server';

const router = Router();

router.post('/', async (req: Request, res: Response) => {
  // TODO: implement
  throw new HTTPError('Not Implemented', 501);
});

export default router;
