import { Router, Request, Response } from 'express';
import { check, HTTPError } from 'lambert-server';
import { EMAIL_REGEX, FieldErrors, trimSpecial, User, config, Snowflake, Member } from '@snowchat/util';
import { generateToken } from './login';
import * as argon2 from 'argon2';

const router: Router = Router();

router.post(
  '/',
  check({
    username: String, // Length: 2-32
    // TODO: check password length in config to prevent a DoS with max length
    password: String, // Length: 8-72
    email: String, // Length: 5-100
    $invite: String,
  }),
  async (req: Request, res: Response) => {
    const { email, username, password, invite } = req.body;

    const currentConfig = config.get();

    // TODO: Block proxies

    const ip = 'TODO'; // TODO: Get IP
    console.log(`[Register] E-Mail: "${req.body.email}" Username: "${req.body.username}" IP: "${ip}"`);

    // TODO: automatically join invite(s)

    // protection against gmail plut and dot tricks
    let correctedEmail: string | null = correctEmail(email);

    // trim special uf8 control characters ex: backspace, newline, etc.
    let correctedUsername: string = trimSpecial(username);

    // check if registration is allowed
    if (!currentConfig.register.allowNewRegistration) {
      throw FieldErrors({
        email: {
          code: 'REGISTRATION_DISABLED',
          message: 'New user registration is disabled',
        },
      });
    }

    // require invite to register -> e.g. for organizations to send invites to their employees
    if (currentConfig.register.requireInvite && !invite) {
      throw FieldErrors({
        email: {
          code: 'INVITE_ONLY',
          message: 'You must be invited to register',
        },
      });
    }

    if (email) {
      if (!correctedEmail)
        throw FieldErrors({
          email: {
            code: 'INVALID_EMAIL',
            message: 'Invalid E-Mail',
          },
        });

      // @ts-ignore
      const exists = await User.findOne({ email: correctedEmail }, { select: {} });

      if (exists) {
        throw FieldErrors({
          email: {
            code: 'EMAIL_ALREADY_REGISTERED',
            message: 'E-Mail Already Registered',
          },
        });
      }
    } else {
      throw FieldErrors({
        email: {
          code: 'BASE_TYPE_REQUIRED',
          message: 'This field is required',
        },
      });
    }

    // TODO: check date of birth

    // TODO: check if config allows multiple accounts per browser

    // TODO: captcha

    let password_hash: string = '';
    try {
      password_hash = await argon2.hash(password);
    } catch (error) {
      console.log('[Invalid Password] Error while hashing password', error);
      throw FieldErrors({ password: { message: 'Invalid Password', code: 'INVALID_PASSWORD' } });
    }

    let discriminator = -1;
    let discriminatorExists;
    for (let tries = 0; tries < 5; tries++) {
      discriminator = Math.randomIntBetween(1, 9999);
      discriminatorExists = await User.findOne({ discriminator, username: correctedUsername }, { select: ['id'] });
      if (!discriminatorExists) break;
    }

    if (discriminatorExists) {
      throw FieldErrors({
        username: {
          code: 'USERNAME_TOO_MANY_USERS',
          message: 'Too many users have this username, please try another one',
        },
      });
    }
    if (discriminator < 0) {
      throw new HTTPError(
        'An error happened while generating your discriminator, please contact the instance administrator',
        500,
      );
    }

    const userJson = {
      id: Snowflake.generate(),
      password_hash: password_hash,
      email: email,
      username: username,
      discriminator: discriminator,
      bot: false,
      developer: false,
      created_at: new Date(),
      status: 'online',
      disabled: false,
      deleted: false,
      guilds: [],
      guild_folders: [],
      custom_status: {
        emoji_id: undefined,
        expires_at: undefined,
        text: undefined,
      },
      valid_tokens_since: new Date(),
    };

    // TODO: Make better .assign method ? '^(?!.*(HTTPError|Server|Date|Promise|Error)).*new '
    // @ts-ignore
    const user: User = User.create([userJson])[0];

    await user.save();

    const instanceGuildId = process.env.INSTANCE_GUILD_ID;
    if (instanceGuildId) await Member.addToGuild(user.id, instanceGuildId);

    return res.json({ token: await generateToken(user.id) });
  },
);

export function correctEmail(email: string): string | null {
  // body parser already checked if it is a valid email
  const parts = <RegExpMatchArray>email.match(EMAIL_REGEX);
  // @ts-ignore
  if (!parts || parts.length < 5) return undefined;
  const domain = parts[5];
  const user = parts[1];

  // TODO: check accounts with uncommon email domains
  if (domain === 'gmail.com' || domain === 'googlemail.com') {
    // Remove dots -> https://support.google.com/mail/answer/7436150
    // Remove plus signs -> https://gmail.googleblog.com/2008/03/2-hidden-ways-to-get-more-from-your.html
    return user.replace(/[.]|(\+.*)/g, '') + '@gmail.com';
  }

  return email;
}

export default router;
