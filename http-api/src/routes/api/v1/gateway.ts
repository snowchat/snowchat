import { Router, Response, Request } from 'express';
import { HTTPError } from 'lambert-server';

const router = Router();

router.get('/', (req: Request, res: Response) => {
  // TODO: implement
  // * NOTE: This is for the future if we need to have multiple Gateway nodes/hosts
  // * We could also put the gateways behing a load balancer like Caddy, Traefik or NGINX
  throw new HTTPError('Not Implemented', 501);
});

export default router;
