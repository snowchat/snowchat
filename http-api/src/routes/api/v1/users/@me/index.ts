import { Router, Request, Response } from 'express';
import { User } from '@snowchat/util';

const router: Router = Router();

router.get('/', async (req: Request, res: Response) => {
  res.json(await User.getPublicUser(req.user_id));
});

export default router;
