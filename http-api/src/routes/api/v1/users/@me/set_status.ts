import { Router, Request, Response } from 'express';
import { User } from '@snowchat/util';
import { check, Tuple } from 'lambert-server';

const router: Router = Router();

router.post(
  '/',
  check({
    status: new Tuple('online', 'idle', 'dnd', 'offline'),
    $custom_status: {
      $emoji_id: String,
      $expires_at: Number, // -1 is never
      $text: String,
    },
  }),
  async (req: Request, res: Response) => {
    console.log('[SetStatus] Setting status to', req.body);
    await User.update({ id: req.user_id }, req.body);
    console.log('[SetStatus] Set status to', req.body);
    // TODO: send event 'user:on_status_change' through RabbitMQ to the Gateway
    return res.json({ message: 'success' });
  },
);

export default router;
