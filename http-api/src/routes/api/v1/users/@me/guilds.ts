import { Member } from '@snowchat/util';
import { Router, Request, Response } from 'express';
import { HTTPError } from 'lambert-server';

const router: Router = Router();

router.get('/', async (req: Request, res: Response) => {
  //throw new HTTPError('Not implemented', 501);
  const members = await Member.find({
    where: { id: req.user_id },
    relations: ['guild', 'guild.channels'],
  });
  const guilds = members.map((m) => m.guild);
  res.json(guilds);
});

// Leave guild
router.delete('/:id', async (req: Request, res: Response) => {
  const { id: guild_id } = req.params;
  if (process.env.INSTANCE_GUILD_ID === guild_id) throw new HTTPError("You can't leave this guild", 400);
  throw new HTTPError('Not implemented', 501);
  /*const guild = await Guild.findOne({ id: guild_id }, { select: ['guild_id'] });

  if (!guild) throw new HTTPError('Guild doesn't exist', 404);
  // TODO: If only one: delete guild
  // TODO: If only one with admin perms: show error
  */
});

export default router;
