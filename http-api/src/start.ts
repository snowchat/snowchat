#!/usr/bin/env node
process.on('uncaughtException', console.error);
process.on('unhandledRejection', console.error);

import { APIServer } from './index';
import cluster from 'cluster';
import os from 'os';
const cores = Number(process.env.threads) || os.cpus().length;

if (
  cluster.isPrimary &&
  process.env.NODE_ENV == 'production' &&
  process.env.SNOWCHAT_THREADED?.toLowerCase() == 'true'
) {
  console.log(`Primary ${process.pid} is running`);

  // Fork workers.
  for (let i = 0; i < cores; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} died, restart worker`);
    cluster.fork();
  });
} else {
  const port = Number(process.env.PORT || '4000');
  const host = process.env.HOST || '0.0.0.0';

  const server = new APIServer({
    port,
    host,
  });
  server.start().catch((err) => console.error('[HTTP API Error]', err));

  // @ts-ignore
  global.server = server;
}
