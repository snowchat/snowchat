import { Server, ServerOptions } from 'lambert-server';
import { CORS } from './middlewares/cors';
import { authentication } from './middlewares/authentication';
import { config } from '@snowchat/util';
import { errorHandler } from './middlewares/errorHandler';
import { bodyParser } from './middlewares/bodyParser';
import { Request, Response, NextFunction } from 'express';

// TODO: Rate limiting

export interface ApiServerOptions extends ServerOptions {}

export class APIServer {
  server: Server;
  constructor(options: Partial<ApiServerOptions>) {
    this.server = new Server({
      ...options,
      // @ts-ignore
      errorHandler: false,
      jsonBody: false,
    });
  }
  async start() {
    config.init();
    this.server.app.use(CORS);
    this.server.app.use(authentication);
    this.server.app.use(bodyParser({ inflate: true, limit: 1024 * 1024 * 10 })); // 2MB limit
    await this.server.registerRoutes(__dirname + '/routes/');
    this.server.app.use(errorHandler);
    await this.server.start();
  }
}
