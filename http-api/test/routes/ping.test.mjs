import supertest from 'supertest';
const request = supertest('http://localhost:4000');
import { assert } from 'chai';

describe('/ping', function () {
  describe('GET', function () {
    it('should return 200 and OK', async function () {
      let response = await request.get('/api/v1/ping');

      assert.equal(response.text, 'OK');
      assert.equal(response.statusCode, 200);
    });
  });
});
