import supertest from 'supertest';
const request = supertest('http://localhost:4000');
import { assert } from 'chai';
import { auth } from '../config.mjs';

describe('/guilds', function () {
  let token;
  let guildId;
  let newChannelId;
  before(async function () {
    let response = await request.post('/api/v1/auth/login').send({
      email: auth.email,
      password: auth.password,
    });
    token = JSON.parse(response.text).token;
  });

  describe('POST', function () {
    it('should return 201 and object with guild ID', async function () {
      let response = await request
        .post('/api/v1/guilds')
        .set('Authorization', 'Bearer ' + token)
        .send({
          name: 'Example Name',
          description: 'Example Description',
          icon_url: null,
          banner_url: null,
          channels: [
            { id: 1, type: 0, name: 'general' },
            { id: 2, type: 0, name: 'off-topic' },
            { id: 3, type: 0, name: 'memes' },
          ],
        });

      guildId = JSON.parse(response.text).id;
      assert.exists(guildId, 'Guild ID exists');

      assert.equal(response.statusCode, 201, 'Status is 201');
    });
    it('should return 201 and a new channel', async function () {
      let response = await request
        .post('/api/v1/guilds/' + guildId + '/channels')
        .set('Authorization', 'Bearer ' + token)
        .send({ type: 0, name: 'general' });

      let channel = JSON.parse(response.text);
      assert.exists(channel.id, 'Channel ID exists');

      assert.equal(response.statusCode, 201, 'Status is 201');
    });
    it('should return 200 and 4 channels', async function () {
      let response = await request
        .get('/api/v1/guilds/' + guildId + '/channels')
        .set('Authorization', 'Bearer ' + token);

      let channels = JSON.parse(response.text);
      assert.exists((channels.length = 4), '4 channels are returned');

      assert.equal(response.statusCode, 200, 'Status is 200');
    });
  });
});
