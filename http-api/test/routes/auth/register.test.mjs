import supertest from 'supertest';
const request = supertest('http://localhost:4000');
import { assert } from 'chai';
import { auth } from '../../config.mjs';

describe('/auth/register', function () {
  describe('POST', function () {
    it('should be able to register', async function () {
      let response = await request.post('/api/v1/auth/register').send({
        username: auth.username,
        password: auth.password,
        email: auth.email
      });

      assert.equal(response.statusCode, 200);
    });
  });
});
