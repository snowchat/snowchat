import supertest from 'supertest';
const request = supertest('http://localhost:4000');
import { assert } from 'chai';
import { auth } from '../../config.mjs';
import jwt from 'jsonwebtoken';

describe('/auth/login', function () {
  describe('POST', function () {
    it('should be able to log in', async function () {
      let response = await request
        .post('/api/v1/auth/login')
        .send({
          email: auth.email,
          password: auth.password
        });

      assert.equal(response.statusCode, 200, 'Status code is 200');

      let json = JSON.parse(response.text)
      assert.exists(json.token, 'Token exists')
      
      assert.doesNotThrow(jwt.decode.bind(this, json.token), 'Token is valid')
      this.token = json.token;
    });
  });
});
