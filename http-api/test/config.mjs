const config = {
  auth: {
    username: 'Unit Test User',
    password: 'p4ssw0rd',
    email: 'unittest@example.com',
  },
};

export const auth = config.auth;

export default config;
