import './util/cleardb.mjs';

import './routes/ping.test.mjs';

import './routes/auth/register.test.mjs';
import './routes/auth/login.test.mjs';

import './routes/guilds.test.mjs';
