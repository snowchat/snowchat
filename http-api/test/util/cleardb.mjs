import pg from 'pg';

const client = new pg.Client('postgres://snowy:example@localhost/snowchat');
await client.connect();

await client.query(`
do
$$
declare
  l_stmt text;
begin
  select 'truncate ' || string_agg(format('%I.%I', schemaname, tablename), ',')
    into l_stmt
  from pg_tables
  where schemaname in ('public');

  execute l_stmt;
end;
$$
`.trim());

await client.end()

console.log('[TEST] Cleared Database')
