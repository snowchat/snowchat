## Note about code style
Before committing your work please run the file `./scripts/prettify.sh`
or `./scripts/prettify.ps1` to prettify all of the files.

This can also be done through VSCode by pressing `Ctrl+P`, then typing
`task prettify` and pressing enter.

## Tests
Before committing your work, if you have changed something in `http-api`
please run the unit tests in `http-api` with `pnpm test` and verify that
the tests run successfully.

## Branches
- `dev`: Merge request this branch for everything
- `master`: Used for releases

## `psql` connection
You can use the following command to connect to the database with `psql`:
```bash
psql postgres://snowy:example@localhost/snowchat
```


---
## `ctop`
`ctop` is a nice tool for interacting and checking the status of your containers.


---
## Ports
The development ports are:
```
HTTP API:   4000
WS Gateway: 4001
Dev Server: 3000
```
**NOTE:**
It all gets reverse proxied through Caddy to the port 443
The dev server also gets reverse proxied through Caddy to the port 3100 for the HMR WebSocket


---
# Docker Debugging
The following command can be useful for debugging errors in Docker containers:
```bash
docker run -it --rm /bin/sh snowchat_bundle
```
