-- @block Remove all users
DELETE
FROM users;

-- @block Get all users
SELECT *
FROM users;

-- @block Get all users
SELECT username||'#'||discriminator,developer,email,status
FROM users;

-- @block Make users named "Test User" developers
UPDATE users
SET developer = TRUE
WHERE username = 'Test User';

-- @block Make the first user named "Test User" have the discriminator 9999
UPDATE users
SET discriminator = 9999
WHERE username = 'Test User';

-- @block Get all guilds
SELECT *
FROM guilds;
-- @block Get all channels
SELECT *
FROM channels;
-- @block Get all members
SELECT *
FROM members;
-- @block Get all messages
SELECT *
FROM messages;

-- @block Remove everything
DELETE
FROM users;
DELETE
FROM guilds;
DELETE
FROM channels;
DELETE
FROM members;
DELETE
FROM messages;
