import { WebSocketI, Payload } from './WebSocket';

export default async function send(socket: WebSocketI, data: Payload) {
  let stringified = JSON.stringify(data);

  return new Promise((res, rej) => {
    socket.send(stringified, (err) => {
      if (err) return rej(err);
      return res(null);
    });
  });
}
