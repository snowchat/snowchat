import http, { IncomingMessage } from 'http';
import WebSocket, { Server as WebSocketServer } from 'ws';
import { checkToken, config } from '@snowchat/util';
import { WebSocketI } from './WebSocket';
import { setupListener } from './listener';

// TODO: Rate limiting

export class GatewayServer {
  public ws: WebSocketServer;
  public port: number;
  public server: http.Server;

  constructor({ port, server, production = false }: { port: number; server?: http.Server; production?: boolean }) {
    this.port = port;

    if (server) this.server = server;
    else
      this.server = http.createServer(function (req, res) {
        res.writeHead(200).end('Online');
      });

    this.server.on('upgrade', (request, socket, head) => {
      console.log('[Gateway] WebSocket Upgrade', request.url);
      // @ts-ignore
      this.ws.handleUpgrade(request, socket, head, (socket) => {
        this.ws.emit('connection', socket, request);
      });
    });

    this.ws = new WebSocketServer({
      maxPayload: 4096,
      noServer: true,
    });
    this.ws.on(
      'connection',
      async function onConnection(this: WebSocketServer, socket: WebSocketI, request: IncomingMessage) {
        if (
          !('sec-websocket-protocol' in request.headers) ||
          !request.headers['sec-websocket-protocol']?.startsWith('snowchat, ')
        ) {
          console.log('[Gateway] Auth header does not exist or is invalid');
          socket.close(4000, 'Invalid Token');
          return;
        }
        try {
          const { decoded } = await checkToken(request.headers['sec-websocket-protocol']?.replace(/^snowchat, /, ''));
          socket.user_id = decoded.id;
        } catch (error) {
          console.log('[Gateway] Token is invalid:', error);
          socket.close(4000, typeof error === 'string' ? error : 'Invalid Token');
          return;
        }
        function onMessage(this: WebSocket, buffer: WebSocket.Data) {
          let data;
          try {
            // @ts-ignore
            data = JSON.parse(buffer);
          } catch (error) {
            console.error('[Gateway] Error while parsing message JSON:', error);
            return;
          }
          console.log('[Gateway] Message received:', data);
          // TODO: Authentication packet and after that send user info with guild list etc.
        }
        socket.on('message', onMessage);
        function onClose(this: WebSocket, code: number, reason: string) {
          this.off('message', onMessage);
        }
        socket.on('close', onClose);
        socket.events = {};

        // TODO: after authentication packet
        await setupListener.call(socket);
      },
    );
    this.ws.on('error', console.error);
  }

  async start(): Promise<void> {
    // @ts-ignore
    await config.init();
    if (!this.server.listening) {
      this.server.listen(this.port);
      console.log(`[Gateway] online on 0.0.0.0:${this.port}`);
    }
  }

  stop() {
    this.server.close();
  }
}
