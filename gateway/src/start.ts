#!/usr/bin/env node
process.on('uncaughtException', console.error);
process.on('unhandledRejection', console.error);

import { GatewayServer } from './index';

const port = Number(process.env.PORT || '4001');

const server = new GatewayServer({
  port,
});
server.start();
