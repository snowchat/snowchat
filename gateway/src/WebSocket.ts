import WebSocket from 'ws';

export interface WebSocketI extends WebSocket {
  user_id: string;
  events: Record<string, Function>;
}

export interface Payload {
  event: string;
  data?: any;
}
