import { Channel } from 'amqplib';
import { WebSocketI } from './WebSocket';
import { RabbitMQ, listenEvent, ListenEventOpts, EventOpts, Member } from '@snowchat/util';
import send from './send';

export async function setupListener(this: WebSocketI) {
  const members = await Member.find({
    where: { id: this.user_id },
    relations: ['guild', 'guild.channels'],
  });
  const guilds = members.map((m) => m.guild);
  const opts: { ack: boolean; channel?: Channel } = {
    ack: true,
  };

  const consumer = consume.bind(this);

  if (!RabbitMQ.connection) throw new Error('RabbitMQ connection does not exist');
  opts.channel = await RabbitMQ.connection.createChannel();
  // @ts-ignore
  opts.channel.queues = {};

  this.events[this.user_id] = await listenEvent(this.user_id, consumer, opts);

  for (const guild of guilds) {
    this.listeners;
    this.events[guild.id] = await listenEvent(guild.id, consumer, opts);

    for (const channel of guild.channels) {
      // TODO: permissions
      this.events[channel.id] = await listenEvent(channel.id, consumer, opts);
    }
  }
}

// TODO: only subscribe for events that are in the connection intents
async function consume(this: WebSocketI, opts: EventOpts) {
  const { data, event } = opts;
  const id = data.id as string;

  const consumer = consume.bind(this);
  const listenOpts = opts as ListenEventOpts;

  // TODO: handle channel/guild creations/deletions

  // permission checking
  switch (event) {
    default:
      // always gets sent
      // Any events not defined in an intent are considered "passthrough" and will always be sent
      break;
  }
  send(this, {
    event,
    data,
  });

  console.log('[Gateway] Received event');
  opts.ack?.();
}
