-- This file is for the VSCode SQLTools extension 

-- @block
SELECT * FROM users;

-- @block
SELECT display_name, status
FROM users;

-- @block
UPDATE users
SET is_developer = TRUE
WHERE username = 'lukas';

-- @block
DELETE FROM users
WHERE username = 'lukas';



-- @block
SELECT *
FROM guilds;

-- @block
SELECT id, display_name
FROM guilds;

-- @block
DELETE FROM guilds
WHERE display_name = 'testing';

-- @block
DELETE FROM guilds
WHERE display_name = 'testing';
