defmodule Snowchat.MixProject do
  use Mix.Project

  def project do
    [
      app: :snowchat_elixir,
      version: "0.1.0",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :crypto, :mnesia, :peerage],
      mod: {Snowchat.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
      # {:plug, "~> 1.11"},         # tried to fix warning that :plug is not a dependency
      {:plug_cowboy, "~> 2.5"},   # for HTTP
      {:jason, "~> 1.2"},         # for JSON serialization/deserialization
      {:riverside, "~> 1.2.6"},   # for WebSockets
      {:gen_registry, "~> 1.0"},  # for managing PIDs
      {:joken, "~> 2.3"},         # for JSON Web Tokens
      {:ecto_sql, "~> 3.0"},      # for Ecto
      {:postgrex, ">= 0.0.0"},    # for Ecto PostgreSQL support
      {:argon2_elixir, "~> 2.4"}, # for password hashing
      {:manifold, "~> 1.4"},      # for faster and more advanced send() function
      {:peerage, "~> 1.0.2"},     # easy clustering, pluggable discovery
      {:snowflake, "~> 1.0.0"},   # Snowflake generation
      # {:redix, ">= 0.0.0"},       # for Redis connection
    ]
  end
end
