import Config

# https://github.com/lyokato/riverside#configurations
config :snowchat_elixir, Snowchat.WS.WebSocketHandler,
  port: {:system, "WS_PORT", 4001},
  path: "/ws",
  # don't accept connections if server already has this number of connections
  max_connections: 10000,
  # force to disconnect a connection if the duration passed. if :infinity is set, do nothing.
  max_connection_age: :infinity,
  codec: Riverside.Codec.JSON,
  show_debug_logs: false,
  transmission_limit: [
    # if 50 frames are sent on a connection
    capacity: 50,
    # in 2 seconds, disconnect it.
    duration: 2000
  ],
  # disconnect if no event comes on a connection during 2 minutes
  idle_timeout: 120_000,
  # TCP SO_REUSEPORT flag
  reuse_port: false

config :snowchat_elixir,
  ecto_repos: [Snowchat.Postgres.Repo],
  log_heartbeat: false,
  websocket_auth_timeout: 10_000

config :snowchat_elixir, :environment, Mix.env()
