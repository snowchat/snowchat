import Config

# cannot use Mix.env() becuase this file is run at runtime and Mix is not available in production enviroments, see dev.exs, prod.exs and test.exs
case Application.get_env(:snowchat_elixir, :environment) do
  :dev ->
    config :snowchat_elixir,
      access_token_secret: System.get_env("ACCESS_TOKEN_SECRET") || "secret",
      refresh_token_secret: System.get_env("REFRESH_TOKEN_SECRET") || "secret",
      port: String.to_integer(System.get_env("PORT") || "4000"),
      # For logging in application.ex:
      ws_port: String.to_integer(System.get_env("WS_PORT") || "4001")

    config :snowchat_elixir, Snowchat.Postgres.Repo,
      url: System.get_env("POSTGRES_URL") || "postgres://postgres:example@localhost/snowchat"

    config :peerage,
      via: Peerage.Via.List,
      node_list: [:snowchat_elixir@localhost],
      log_results: false

  :prod ->
    # Do not print debug messages in production
    config :logger, level: :info

    config :snowchat_elixir,
      access_token_secret:
        System.get_env("ACCESS_TOKEN_SECRET") ||
          raise("""
          Environment variable ACCESS_TOKEN_SECRET is missing!
          """),
      refresh_token_secret:
        System.get_env("REFRESH_TOKEN_SECRET") ||
          raise("""
          Environment variable REFRESH_TOKEN_SECRET is missing!
          """),
      port: String.to_integer(System.get_env("PORT") || "4000"),
      # For logging in application.ex:
      ws_port: String.to_integer(System.get_env("WS_PORT") || "4001")

    config :snowchat_elixir, Snowchat.Postgres.Repo,
      url:
        System.get_env("POSTGRES_URL") ||
          raise("""
          Environment variable POSTGRES_URL is missing!
          """)

    config :peerage,
      via: Peerage.Via.Dns,
      dns_name:
        System.get_env("SERVICE_NAME") ||
          raise("""
          Environment variable SERVICE_NAME is missing!
          """),
      app_name: "snowchat_elixir"

  _ ->
    config :snowchat_elixir,
      access_token_secret: System.get_env("ACCESS_TOKEN_SECRET") || "secret",
      refresh_token_secret: System.get_env("REFRESH_TOKEN_SECRET") || "secret",
      port: String.to_integer(System.get_env("PORT") || "4000"),
      # For logging in application.ex:
      ws_port: String.to_integer(System.get_env("WS_PORT") || "4001")

    config :snowchat_elixir, Snowchat.Postgres.Repo,
      url: System.get_env("POSTGRES_URL") || "postgres://postgres:example@localhost/snowchat"

    config :peerage,
      via: Peerage.Via.List,
      node_list: [:snowchat_elixir@localhost],
      log_results: false
end

config :snowflake,
  machine_id: 0,
  # Don't change after you decide what the epoch is
  # The following number is the first millisecond of 2021 (relative to the first millisecond of 1970)
  epoch: 1_609_459_200_000
