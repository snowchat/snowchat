defmodule Snowchat.Util do
  def typeof(data) do
    cond do
      is_float(data)     -> "float"
      is_number(data)    -> "number"
      is_atom(data)      -> "atom"
      is_boolean(data)   -> "boolean"
      is_binary(data)    -> "binary"
      is_function(data)  -> "function"
      is_list(data)      -> "list"
      is_tuple(data)     -> "tuple"
      is_bitstring(data) -> "bitstring"
      is_map(data)       -> "map"
      is_nil(data)       -> "nil"
      true               -> "other"
    end
  end
  def remove_prefix(full, prefix) do
    base = byte_size(prefix)
    <<_ :: binary-size(base), rest :: binary>> = full
    rest
  end
  def create_random_string(length \\ 20) do
    symbols = '0123456789abcdef'
    for _ <- 1..length, into: "", do: <<Enum.random(symbols)>>
  end
end
