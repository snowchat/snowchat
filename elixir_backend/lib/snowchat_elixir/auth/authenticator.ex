defmodule Snowchat.Auth.Authenticator do
  alias Snowchat.Auth.AccessToken
  alias Snowchat.Postgres
  alias Postgres.Repo
  alias Postgres.Schemas.User

  def authenticate(nil) do
    {:error, :nil_token}
  end

  def authenticate(access_token) do
    case AccessToken.verify_and_validate(access_token) do
      {:ok, accessClaims} ->
        {:ok, accessClaims["user_id"]}

      {:error, error} ->
        {:error, error}

      _ ->
        {:error, :invalid_token}
    end
  end
end
