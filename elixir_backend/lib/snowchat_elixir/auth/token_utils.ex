defmodule Snowchat.Auth.TokenUtils do
  alias Snowchat.Auth.RefreshToken
  alias Snowchat.Auth.AccessToken
  alias Snowchat.Postgres
  alias Postgres.Schemas.User
  alias Postgres.Repo
  alias Postgres.Schemas.RefreshToken, as: RefreshTokenSchema

  def add_token_to_db(refresh_token, user_id) do
    Postgres.Repo.insert(
      RefreshTokenSchema.changeset(
        %RefreshTokenSchema{},
        %{
          token: refresh_token,
          user: user_id
        }
      )
    )
  end

  def check_token_in_db(refresh_token) do
    import Ecto.Query

    RefreshTokenSchema
    |> where([t], t.token == ^refresh_token)
    |> Repo.one()
  end

  def remove_token_from_db(refresh_token) do
    import Ecto.Query

    RefreshTokenSchema
    |> where([t], t.token == ^refresh_token)
    |> Repo.delete_all()
  end

  def check_expired(token) do
    case Joken.peek_claims(token) do
      {:ok, claims} ->
        {:ok, claims["exp"] < Joken.current_time()}

      {:error, reason} ->
        {:error, reason}
    end
  end

  def refresh_tokens(refresh_token!) do
    refresh_token! = refresh_token! || ""

    case RefreshToken.verify_and_validate(refresh_token!) do
      {:ok, refreshClaims} ->
        user = Repo.get(User, refreshClaims["user_id"])

        if user && !user.reason_for_ban do
          {:new_tokens, user.id, create_tokens(user.id), user}
        end

      _ ->
        nil
    end
  end

  def create_tokens(user_id) do
    %{
      accessToken: AccessToken.generate_and_sign!(%{"user_id" => user_id}),
      refreshToken: RefreshToken.generate_and_sign!(%{"user_id" => user_id})
    }
  end
end
