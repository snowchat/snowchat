defmodule Snowchat.HTTP.Routes.ApiRouter do
  use Plug.Router

  require Logger

  plug(:match)

  plug(Plug.Parsers,
    parsers: [:json],
    pass: ["application/json"],
    json_decoder: Jason
  )

  plug(:dispatch)

  get "example" do
    send_resp(conn, 200, "This is sample text fetched from the Elixir API")
  end

  get "stats" do
    {:ok, stats_str} =
      Jason.encode(%{
        guild_process_count: GenRegistry.count(Snowchat.Guild)
      })

    send_resp(conn, 200, stats_str)
  end

  forward("/auth", to: Snowchat.HTTP.Routes.Api.AuthRouter)
  forward("/users", to: Snowchat.HTTP.Routes.Api.UserRouter)
  forward("/channels", to: Snowchat.HTTP.Routes.Api.ChannelRouter)
  forward("/guilds", to: Snowchat.HTTP.Routes.Api.GuildRouter)

  match _ do
    send_resp(conn, 404, "404 Not Found")
  end
end
