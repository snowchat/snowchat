defmodule Snowchat.HTTP.Routes.Api.ChannelRouter do
  use Plug.Router

  require Logger

  plug(:match)

  plug(Snowchat.HTTP.Plugs.AuthPlug)

  plug(Plug.Parsers,
    parsers: [:json],
    pass: ["application/json"],
    json_decoder: Jason
  )

  plug(:dispatch)

  alias Snowchat.WS.WebSocketHelpers

  # TODO: Move messages path to another router

  post "/:channel_id/messages" do
    channel_id = conn.path_params["channel_id"]
    user_id = conn.assigns[:user_id]
    data = conn.body_params

    # TODO: validate channel id

    # TODO: if guild check permission to send messages
    # TODO: if DM check blocked

    # TODO: check perms for components
    # TODO: check perms for mention_everyone
    # TODO: check perms for mention_roles
    # TODO: check perms for referenced_message

    # create full message
    full_msg = %{
      author: user_id,
      content: data["content"],
      # TODO: check perms
      attachments: data["attachments"] || [],
      # TODO: check perms
      embeds: data["embeds"] || [],
      # TODO: check perms
      pinned: data["pinned"] || false,
      # Buttons etc.
      components: data["components"] || [],
      # @everyone, @here
      mention_everyone: data["mention_everyone"] || false,
      # <@&872423636978507856>
      mention_roles: data["mention_roles"] || [],
      # reply
      referenced_message: data["referenced_message"] || nil,
      type: 0
    }

    # TODO: save to DB

    # broadcast to subscribed clients

    _ = """
    case GenRegistry.lookup(Snowchat.Guild, server_id) do
      {:ok, pid} ->
        # Do something with pid
        send(pid, %{
          event: "send_message",
          data: %{
            channel_id: channel_id,
            message: data["message"]
          }
        })

      {:error, :not_found} ->
        # id isn't bound to any running process.
        Logger.warn("# {server_id} isn't bound to any running process.")
    end
    """

    # TODO: Use GenRegistry

    WebSocketHelpers.deliver_channel(channel_id, %{
      event: "message:receive",
      data: full_msg
    })

    # TODO: respond with full message data
    send_resp(conn, 201, Jason.encode!(full_msg))
  end
end
