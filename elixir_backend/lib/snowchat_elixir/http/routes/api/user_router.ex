defmodule Snowchat.HTTP.Routes.Api.UserRouter do
  use Plug.Router

  require Logger

  alias Snowchat.Postgres.Mutations.Users, as: UserMutations
  alias Snowchat.WS.WebSocketHelpers

  plug(:match)

  plug(Snowchat.HTTP.Plugs.AuthPlug)

  plug(Plug.Parsers,
    parsers: [:json],
    pass: ["application/json"],
    json_decoder: Jason
  )

  plug(:dispatch)

  post "/:user_id/set_status" do
    user_id = conn.path_params["user_id"]

    if user_id === "@me" do
      _ = """
      Request Schema:
      %{
        "status" => "idle"
      }
      """

      user_id = conn.assigns[:user_id]

      data = conn.body_params

      case data["status"] do
        status
        when is_binary(status) and
               status in [
                 "online",
                 "idle",
                 "dnd",
                 "invisible"
               ] ->
          UserMutations.edit_profile(user_id, %{
            status: status
          })

          WebSocketHelpers.deliver_user(user_id, %{
            event: "user:on_status_change",
            data: status
          })

          send_resp(conn, 200, "")

        _ ->
          send_resp(
            conn,
            400,
            Jason.encode!(%{
              error: "Unknown status"
            })
          )
      end
    end
  end
end
