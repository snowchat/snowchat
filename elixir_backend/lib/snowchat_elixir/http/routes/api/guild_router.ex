defmodule Snowchat.HTTP.Routes.Api.GuildRouter do
  use Plug.Router

  require Logger

  plug(:match)

  plug(Snowchat.HTTP.Plugs.AuthPlug)

  plug(Plug.Parsers,
    parsers: [:json],
    pass: ["application/json"],
    json_decoder: Jason
  )

  plug(:dispatch)

  post "/:guild_id/" do
    guild_id = conn.path_params["guild_id"]
    user_id = conn.assigns[:user_id]
    data = conn.body_params

    # TODO: Create Guild

    send_resp(
      conn,
      400,
      Jason.encode!(%{
        error: "This API is not yet implemented"
      })
    )

    # send_resp(conn, 201, Jason.encode!(full_guild_data))
  end

  get "/:guild_id/list" do
    guild_id = conn.path_params["guild_id"]

    if guild_id !== "@me" do
      send_resp(
        conn,
        400,
        Jason.encode!(%{
          error: "You can only use the \"@me\" guild id for the /list guild endpoint!"
        })
      )
    end

    user_id = conn.assigns[:user_id]
    data = conn.body_params

    # TODO: Get Guild List From DB

    send_resp(
      conn,
      200,
      Jason.encode!(%{
        guilds: [
          %{
            "name" => "Official Snowchat",
            "id" => "78823607425302528",
            "owner_id" => "78823607425302528",
            "member_count" => 123,
            "icon_url" => "https://images.unsplash.com/photo-1623887197322-7d79d2792e68?w=256",
            "vanity_url_code" => "snowchat",
            "channels" => [
              %{
                "guild_id" => "78823607425302528",
                "id" => "78823607425302528",
                "last_message_id" => "78823607425302528",
                "category_id" => "78823607425302528",
                "position" => 0,
                "name" => "General Channel!",
                "topic" => "Here you can discuss about Snowchat",
                "type" => "text"
              }
            ]
          }
        ]
      })
    )

    # send_resp(conn, 201, Jason.encode!(full_guild_data))
  end
end
