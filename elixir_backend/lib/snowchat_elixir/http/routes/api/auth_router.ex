defmodule Snowchat.HTTP.Routes.Api.AuthRouter do
  use Plug.Router

  require Logger

  alias Snowchat.Auth
  alias Auth.RefreshToken
  alias Auth.AccessToken
  alias Auth.TokenUtils
  alias Snowchat.Postgres.Utils.UserUtils

  plug(:match)

  plug(Plug.Parsers,
    parsers: [:json],
    pass: ["application/json"],
    json_decoder: Jason
  )

  plug(:dispatch)

  post "refresh_token" do
    data = conn.body_params
    Logger.debug("Refresh token")

    """
    Request schema:
    %{
      "refreshToken" => "aaaa.bbbbbb.cccc"
    }
    Response schema:
    %{
      "refreshToken" => "aaaa.bbbbbb.cccc",
      "accessToken" => "aaaa.bbbbbb.cccc"
    }
    """

    # TODO: check request schema

    oldRefreshToken = data["refreshToken"]

    status =
      if TokenUtils.check_token_in_db(oldRefreshToken) do
        :does_not_exist
      else
        case TokenUtils.check_expired(oldRefreshToken) do
          {:ok, true} -> :expired
          {:ok, false} -> :ok
          {:error, _reason} -> :ok
        end
      end

    case status do
      :expired ->
        send_resp(conn, 400, "{\"status\": \"REFRESH_TOKEN_EXPIRED\"}")

      :does_not_exist ->
        send_resp(conn, 400, "{\"status\": \"REFRESH_TOKEN_DOES_NOT_EXIST\"}")

      :ok ->
        case TokenUtils.refresh_tokens(oldRefreshToken) do
          {:new_tokens, user_id, tokens, user} ->
            TokenUtils.add_token_to_db(tokens.refreshToken, user_id)

            resp_data = %{
              refreshToken: tokens.refreshToken,
              accessToken: tokens.accessToken
            }

            {:ok, resp_data_str} = Jason.encode(resp_data)
            send_resp(conn, 200, resp_data_str)

          _ ->
            send_resp(conn, 400, "{\"error\": \"Something went wrong\"}")
        end
    end
  end

  post "get_refresh_token" do
    data = conn.body_params
    Logger.debug("Get refresh token")

    _ = """
    Request schema:
    %{
      "username" => "my_username",
      "password" => "my password is a secret"
    }
    Response schema:
    %{
      "refreshToken" => "aaaa.bbbbbb.cccc",
      "accessToken" => "aaaa.bbbbbb.cccc"
    }
    """

    # TODO: check request schema

    user = UserUtils.get_by_username(data["username"])

    if user do
      if UserUtils.check_password(user.password, data["password"]) do
        tokens = TokenUtils.create_tokens(user.id)

        TokenUtils.add_token_to_db(tokens.refreshToken, user.id)

        resp_data = %{
          refreshToken: tokens.refreshToken,
          accessToken: tokens.accessToken
        }

        {:ok, resp_data_str} = Jason.encode(resp_data)
        send_resp(conn, 200, resp_data_str)
      else
        resp_data = %{
          error: "Wrong password"
        }

        {:ok, resp_data_str} = Jason.encode(resp_data)
        send_resp(conn, 401, resp_data_str)
      end
    else
      resp_data = %{
        error: "User not found"
      }

      {:ok, resp_data_str} = Jason.encode(resp_data)
      send_resp(conn, 404, resp_data_str)
    end
  end

  post "register" do
    data = conn.body_params
    Logger.debug("Create user with username " <> data["username"])

    # TODO: check request schema

    case UserUtils.create_user(data["username"], data["password"], data["display_name"]) do
      {:ok, _inserted} ->
        send_resp(conn, 201, "201 Created")

      {:error, changeset} ->
        errors = Map.new(changeset.errors, fn {k, {message, _}} -> {k, message} end)

        resp_data = %{changeset_errors: errors}

        {:ok, resp_data_str} = Jason.encode(resp_data)
        send_resp(conn, 400, resp_data_str)
    end
  end

  # TODO: delete user
end
