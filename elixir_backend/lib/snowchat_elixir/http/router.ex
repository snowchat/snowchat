defmodule Snowchat.HTTP.Router do
  use Plug.Router

  plug Plug.Logger, log: :debug
  plug :match
  plug :dispatch

  forward "/api", to: Snowchat.HTTP.Routes.ApiRouter

  match _ do
    send_resp(conn, 400, "you shouldn't be here")
  end
end
