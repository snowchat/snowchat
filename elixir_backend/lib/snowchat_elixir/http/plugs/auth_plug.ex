defmodule Snowchat.HTTP.Plugs.AuthPlug do
  @moduledoc """
  A module plug that verifies the bearer token in the request headers and
  assigns `:user_id`. The authorization header value may look like
  `Bearer xxxxxxx`.
  """
  import Plug.Conn

  @spec init(any) :: any
  def init(opts), do: opts

  defp authenticate({conn, "Bearer " <> token}) do
    case Snowchat.Auth.Authenticator.authenticate(token) do
      {:ok, user_id} ->
        assign(conn, :user_id, user_id)

      {:error, err} ->
        send_401(conn, %{error: err})
    end
  end

  defp authenticate({conn, _}) do
    send_401(conn, %{message: "Please make sure you have a valid authorization header"})
  end

  defp send_401(
         conn,
         data
       ) do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(401, Jason.encode!(data))
    |> halt
  end

  defp get_auth_header(conn) do
    case get_req_header(conn, "authorization") do
      [token] -> {conn, token}
      _ -> {conn}
    end
  end

  @spec call(Plug.Conn.t(), any) :: Plug.Conn.t()
  def call(%Plug.Conn{request_path: _path} = conn, _opts) do
    conn
    |> get_auth_header
    |> authenticate
  end
end
