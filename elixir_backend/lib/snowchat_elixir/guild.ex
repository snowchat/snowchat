defmodule Snowchat.Guild do

  require Logger

  use GenServer

  # Client

  def start_link(default) when is_map(default) do
    GenServer.start_link(__MODULE__, default)
  end

  # Server (callbacks)

  @impl true
  def init(initial_state) do
    {:ok, initial_state}
  end

  @impl true
  def handle_call({:add_session, {user_id}}, _from, state) do
    {:noreply, state}
  end

  @impl true
  def handle_call({:remove_session, {user_id}}, _from, state) do
    {:noreply, state}
  end

  @impl true
  def handle_call({:subscribe_channel, {user_id}}, _from, state) do
    {:noreply, state}
  end

  @impl true
  def handle_call({:unsubscribe_channel, {user_id, channel_id}}, _from, state) do
    {:noreply, state}
  end

  @impl true
  def handle_call({:send_message, {channel_id, message}}, _from, state) do
    # TODO: Add message to database
    # TODO: Validate channel
    Enum.filter(state.sessions, fn session ->
      channel_id in session.subscribed_channels
    end)
    |> Manifold.send({:send_message, channel_id, message})
    {:noreply, state}
  end

  @impl true
  def handle_info({:exit, reason}, state) do
    Logger.info("Guild #{state.id} \":exit\" received, exiting")
    {:stop, reason || :normal, state}
  end

end
