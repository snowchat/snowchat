defmodule Snowchat.Postgres.Mutations.Users do
  import Ecto.Query, warn: false

  alias Snowchat.Postgres
  alias Postgres.Schemas.User
  alias Postgres.Repo

  @spec edit_profile(
          any,
          :invalid | %{optional(:__struct__) => none, optional(atom | binary) => any}
        ) :: any
  def edit_profile(user_id, data) do
    # Old code, queried db before updating:
    # user_id
    # |> (&Repo.get(User, &1)).()
    # |> User.changeset(data)
    # |> Repo.update()

    klist = Keyword.new(data) # map to keyword list

    from(u in User)
    |> where([u], u.id == ^user_id)
    |> update(
      set: ^klist
    )
    |> Repo.update_all([])
  end

  @spec delete(any) :: any
  def delete(user_id) do
    %User{id: user_id} |> Repo.delete()
  end

  @spec ban_user_global(any, any) :: any
  def ban_user_global(user_id, reason_for_ban) do
    from(u in User)
    |> where([u], u.id == ^user_id)
    |> update(
      set: [
        reason_for_ban: ^reason_for_ban
      ]
    )
    |> Repo.update_all([])
  end

  @spec set_offline(any) :: any
  def set_offline(user_id) do
    from(u in User)
    |> where([u], u.id == ^user_id)
    |> update(
      set: [
        last_online: fragment("now()")
      ]
    )
    |> Repo.update_all([])
  end

  @spec create_user(nil | maybe_improper_list | map) :: any
  def create_user(user) do
    Repo.insert(
      User.changeset(
        %User{},
        %{
          username: user[:username],
          password: user[:password],
          display_name:
            if(is_nil(user[:display_name]) or String.trim(user[:display_name]) == "",
              do: "Snowchat User",
              else: user[:display_name]
            ),
          avatar_url: user[:avatar_url],
          banner_url: user[:banner_url],
          bio: user[:bio],
          status: user[:status] || "online",
          custom_status: user[:custom_status],
          is_developer: false
        }
      ),
      returning: true
    )
  end
end
