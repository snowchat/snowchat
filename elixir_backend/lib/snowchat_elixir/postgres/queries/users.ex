defmodule Snowchat.Postgres.Queries.Users do
  import Ecto.Query

  alias Snowchat.Postgres
  alias Postgres.Schemas.User

  @spec limit_one(any) :: Ecto.Query.t()
  def limit_one(query) do
    limit(query, [], 1)
  end

  @spec filter_by_id(any, any) :: Ecto.Query.t()
  def filter_by_id(query, user_id) do
    where(query, [u], u.id == ^user_id)
  end

  @spec filter_by_username(any, any) :: Ecto.Query.t()
  def filter_by_username(query, username) do
    where(query, [u], u.username == ^username)
  end

  @spec list_users :: Ecto.Query.t()
  def list_users() do
    User
    |> where([u], not is_nil(u.username))
  end

  @spec search_username(binary) :: Ecto.Query.t()
  def search_username(<<first_letter>> <> rest) when first_letter == ?@ do
    search_username(rest)
  end

  def search_username(start_of_username) do
    search_str = start_of_username <> "%"

    User
    |> where([u], ilike(u.username, ^search_str))
    |> limit([], 15)
  end
end
