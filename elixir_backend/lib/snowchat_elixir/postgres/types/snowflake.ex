defmodule Snowchat.Postgres.Types.Snowflake do
  use Ecto.Type

  @spec autogenerate :: integer
  def autogenerate() do
    {:ok, id} = Snowflake.next_id()
    id
  end

  def type, do: :integer

  # external to internal
  def cast(id) when is_integer(id) do
    {:ok, id}
  end
  def cast(_), do: :error

  # database to internal
  def load(id) when is_integer(id) do
    {:ok, id}
  end
  def load(_), do: :error

  # internal to database
  def dump(id) when is_integer(id) do
    {:ok, id}
  end
  def dump(_), do: :error
end
