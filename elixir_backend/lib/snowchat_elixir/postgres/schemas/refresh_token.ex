defmodule Snowchat.Postgres.Schemas.RefreshToken do
  use Ecto.Schema

  alias Snowchat.Postgres
  alias Postgres.Schemas.User
  alias Snowchat.Postgres.Types.Snowflake, as: SnowflakeType

  @primary_key {:id, SnowflakeType, autogenerate: true}
  schema "refresh_tokens" do
    field :token, :string
    belongs_to(:user, User, foreign_key: :user_id, type: SnowflakeType)

    timestamps(type: :utc_datetime_usec)
  end

  import Ecto.Changeset

  @required ~w(token user_id)a

  def changeset(refresh_token, attrs \\ %{}) do
    refresh_token
    |> cast(attrs, @required)
    |> validate_required(@required)
  end
end
