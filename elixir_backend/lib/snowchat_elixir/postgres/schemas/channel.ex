defmodule Snowchat.Postgres.Schemas.Channel do
  use Ecto.Schema
  import Ecto.Changeset

  alias Snowchat.Postgres.Schemas.Guild
  alias Snowchat.Postgres.Schemas.Message
  alias Snowchat.Postgres.Types.Snowflake, as: SnowflakeType

  @primary_key {:id, SnowflakeType, autogenerate: true}
  schema "channels" do
    field(:channel_name, :string)
    field(:topic, :string, default: "")
    field(:channel_type, :string, default: "text")

    has_one(:last_message, Message)
    belongs_to(:guild, Guild, foreign_key: :guild_id, type: SnowflakeType)

    timestamps(type: :utc_datetime_usec)
  end

  @required ~w(topic channel_type)a
  @optional ~w(channel_name guild_id owner_id)a

  def changeset(channel, params \\ %{}) do
    channel
    |> cast(params, @optional ++ @required)
    |> validate_required(@required)
    |> validate_format(:channel_name, ~r/^([a-z0-9\-]){4,15}$/)
    |> validate_length(:topic, min: 3, max: 300)
    |> validate_inclusion(:channel_type, ["text", "dm", "announcement_text", "voice", "stage", "kanban"])
  end
end
