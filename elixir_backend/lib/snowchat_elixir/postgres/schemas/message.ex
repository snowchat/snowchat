defmodule Snowchat.Postgres.Schemas.Message do
  use Ecto.Schema
  import Ecto.Changeset

  alias Snowchat.Postgres.Schemas.Channel
  alias Snowchat.Postgres.Schemas.Guild
  alias Snowchat.Postgres.Schemas.User
  alias Snowchat.Postgres.Schemas.Message
  alias Snowchat.Postgres.Types.Snowflake, as: SnowflakeType

  @primary_key {:id, SnowflakeType, autogenerate: true}
  schema "messages" do
    field(:content, :string)
    field(:message_type, :string, default: "text")

    belongs_to(:channel, Channel, foreign_key: :channel_id, type: SnowflakeType)
    belongs_to(:user, User, foreign_key: :author_id, type: SnowflakeType)
    has_one(:referenced_message, Message)

    timestamps(type: :utc_datetime_usec)
  end

  @required ~w(author_id message_type)a
  @optional ~w(guild_id channel_id content reply)a

  def changeset(message, params \\ %{}) do
    message
    |> cast(params, @optional ++ @required)
    |> validate_required(@required)
    |> validate_inclusion(:message_type, ["default", "call", "channel_pinned_message", "guild_member_join", "boost", "boost_tier_1", "boost_tier_2", "boost_tier_3", "channel_follow_add", "reply"])
  end
end
