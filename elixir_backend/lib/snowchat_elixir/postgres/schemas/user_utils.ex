defmodule Snowchat.Postgres.Utils.UserUtils do
  alias Snowchat.Postgres
  alias Postgres.Queries

  alias Postgres.Repo

  alias Postgres.Schemas.User, as: UserSchema

  def create_user(username, password, display_name) do
    password_hash = Argon2.hash_pwd_salt(password)

    Postgres.Mutations.Users.create_user(%{
      username: username,
      password: password_hash,
      display_name: display_name,
      avatar_url: nil,
      banner_url: nil,
      bio: nil,
      status: nil,
      custom_status: ""
    })
  end

  def check_password(original_password, test_password) do
    Argon2.verify_pass(test_password, original_password)
  end

  def list_users() do
    Queries.Users.list_users()
    |> Repo.all()
  end

  def get_by_username(username) do
    UserSchema
    |> Queries.Users.filter_by_username(username)
    |> Queries.Users.limit_one()
    |> Repo.one()
  end

  def get_by_id(user_id) do
    Repo.get(User, user_id)
  end

  import Ecto.Query, only: [select: 3]

  def get_status_by_id(user_id) do
    UserSchema
    |> Queries.Users.filter_by_id(user_id)
    |> Queries.Users.limit_one()
    |> select([u], u.status)
    |> Repo.one()
  end
end
