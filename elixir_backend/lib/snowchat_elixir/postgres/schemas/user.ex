defmodule Snowchat.Postgres.Schemas.User do
  use Ecto.Schema

  alias Snowchat.Postgres.Schemas.Guild
  alias Snowchat.Postgres.Schemas.RefreshToken
  alias Snowchat.Postgres.Types.Snowflake, as: SnowflakeType

  @primary_key {:id, SnowflakeType, autogenerate: true}
  schema "users" do
    field(:username, :string)
    field(:password, :string)
    field(:display_name, :string)
    field(:bio, :string, default: "")
    field(:avatar_url, :string)
    field(:banner_url, :string)
    field(:status, :string, default: "online") # can be "online", "idle", "dnd" or "invisible"
    field(:custom_status, :string)
    field(:reason_for_ban, :string, null: true)
    field(:last_online, :utc_datetime_usec, null: true)
    field(:last_ip, :string, null: true)
    field(:is_developer, :boolean, default: false)

    has_many(:owned_guilds, Guild)
    has_many(:refresh_tokens, RefreshToken)

    timestamps(type: :utc_datetime_usec)
  end

  import Ecto.Changeset

  @required ~w(username password display_name status is_developer)a
  @optional ~w(avatar_url banner_url reason_for_ban last_online last_ip custom_status bio)a

  @spec changeset(
          {map, map}
          | %{
              :__struct__ => atom | %{:__changeset__ => map, optional(any) => any},
              optional(atom) => any
            },
          :invalid | %{optional(:__struct__) => none, optional(atom | binary) => any}
        ) :: Ecto.Changeset.t()
  def changeset(user, attrs \\ %{}) do
    user
    |> cast(attrs, @optional ++ @required)
    |> validate_required(@required)
    |> update_change(:display_name, &String.trim/1)
    |> validate_format(:username, ~r/^([a-z0-9\-_,.]){4,15}$/)
    |> validate_length(:display_name, min: 3, max: 20)
    |> validate_length(:bio, min: 0, max: 160)
    |> unique_constraint(:username)
  end
end
