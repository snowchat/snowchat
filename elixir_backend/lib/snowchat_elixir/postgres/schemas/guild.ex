defmodule Snowchat.Postgres.Schemas.Guild do
  use Ecto.Schema

  alias Snowchat.Postgres.Schemas.User
  alias Snowchat.Postgres.Schemas.Channel
  alias Snowchat.Postgres.Types.Snowflake, as: SnowflakeType

  @primary_key {:id, SnowflakeType, autogenerate: true}
  schema "guilds" do
    field(:display_name, :string)
    field(:icon_url, :string)
    field(:banner_url, :string)
    field(:bio, :string, default: "")

    belongs_to(:user, User, foreign_key: :owner_id, type: SnowflakeType)
    has_many(:channels, Channel)

    timestamps(type: :utc_datetime_usec)
  end

  import Ecto.Changeset

  @required ~w(id display_name owner_id)a
  @optional ~w(icon_url banner_url bio)a

  def changeset(guild, attrs \\ %{}) do
    guild
    |> cast(attrs, @optional ++ @required)
    |> validate_required(@required)
  end
end
