defmodule Snowchat.Postgres.Repo do
  use Ecto.Repo,
    otp_app: :snowchat_elixir,
    adapter: Ecto.Adapters.Postgres
end
