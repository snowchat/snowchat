defmodule Snowchat.WS.WebSocketHandler do
  require Logger

  # set 'otp_app' param like Ecto.Repo
  use Riverside, otp_app: :snowchat_elixir

  alias Snowchat.Postgres.Utils.UserUtils

  def ws_preflight_checks(req) do
    ip_addr = req.headers["x-real-ip"] || req.headers["x-forwarded-for"]
    # The following gets returned
    String.starts_with?(req.headers["sec-websocket-protocol"], "snowchat, ") and
      not blocked_ip(ip_addr)
  end

  def blocked_ip(ip) do
    blocked_ips = []
    Enum.member?(blocked_ips, ip)
  end

  def get_token(req) do
    Snowchat.Util.remove_prefix(req.headers["sec-websocket-protocol"], "snowchat, ")
  end

  @impl Riverside
  def authenticate(req) do
    if ws_preflight_checks(req) do
      token = get_token(req)

      case Snowchat.Auth.Authenticator.authenticate(token) do
        {:ok, user_id} ->
          state = %{}
          {:ok, user_id, state}

        {:error, error_msg} ->
          error =
            auth_error_with_code(401)
            |> put_auth_error_header("X-Auth-Error", Atom.to_string(error_msg))

          {:error, error}
      end
    else
      error = auth_error_with_code(400)
      {:error, error}
    end
  end

  @impl Riverside
  def handle_message(msg, session, state) do
    if msg["event"] !== "ping" do
      Logger.warn("Got message: #{inspect(msg)}")
    end

    {:ok, session, state}
  end

  @impl Riverside
  def init(session, state) do
    # This happens after authenticate/1

    status = UserUtils.get_status_by_id(session.user_id)

    deliver_user(session.user_id, %{
      event: "user:on_status_change",
      data: status
    })

    {:ok, session, state}
  end

  @impl Riverside
  def terminate(_reason, _session, _state) do
    :ok
  end
end
