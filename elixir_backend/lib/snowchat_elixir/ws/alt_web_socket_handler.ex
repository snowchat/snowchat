defmodule Snowchat.WS.AltWebSocketHandler do
  require Logger

  @type state :: %__MODULE__{
          awaiting_init: boolean(),
          user_id: String.t(),
          encoding: :etf | :json,
          compression: nil | :zlib
        }

  defstruct awaiting_init: true,
            user_id: nil,
            encoding: nil,
            compression: nil,
            callers: []

  @behaviour :cowboy_websocket

  ###############################################################
  ## initialization boilerplate

  @impl true
  def init(request, _state) do
    props = :cowboy_req.parse_qs(request)

    compression =
      case :proplists.get_value("compression", props) do
        p when p in ["zlib_json", "zlib"] -> :zlib
        _ -> nil
      end

    encoding =
      case :proplists.get_value("encoding", props) do
        "etf" -> :etf
        _ -> :json
      end

    state = %__MODULE__{
      awaiting_init: true,
      user_id: nil,
      encoding: encoding,
      compression: compression,
      callers: get_callers(request)
    }

    {:cowboy_websocket, request, state}
  end

  @auth_timeout Application.compile_env(:snowchat_elixir, :websocket_auth_timeout)

  @impl true
  def websocket_init(state) do
    Process.send_after(self(), :auth_timeout, @auth_timeout)
    Process.put(:"$callers", state.callers)

    {:ok, state}
  end

  #######################################################################
  ## API

  @typep command :: :cow_ws.frame() | {:shutdown, :normal}
  @typep call_result :: {[command], state}

  # exit
  def exit(pid), do: send(pid, :exit)
  @spec exit_impl(state) :: call_result
  defp exit_impl(state) do
    # note the remote webserver will then close the connection.  The
    # second command forces a shutdown in case the client is a jerk and
    # tries to DOS us by holding open connections.
    # frontend expects 4003
    ws_push([{:close, 4003, "killed by server"}, shutdown: :normal], state)
  end

  # auth timeout
  @spec auth_timeout_impl(state) :: call_result
  defp auth_timeout_impl(state) do
    if state.awaiting_init do
      ws_push([{:close, 1000, "authorization"}, shutdown: :normal], state)
    else
      ws_push(nil, state)
    end
  end

  # transitional remote_send message
  def remote_send(socket, message), do: send(socket, {:remote_send, message})

  defp remote_send_impl(message, state) do
    {[prepare_socket_msg(message, state)], state}
  end

  def validate(message_map, state) do
    test = false

    # check if is nil
    test = test or is_nil(message_map["event"])
    # check if is nil
    test = test or is_nil(message_map["data"])
    # check if is not string
    test = test or not is_binary(message_map["event"])
    # check if is not map
    test = test or not is_map(message_map["data"])

    if test do
      {:error, :validation}
    else
      message = %{
        event: message_map["event"],
        data: message_map["data"]
      }
      {:ok, message}
    end
  end

  @impl true
  def websocket_handle({:text, "ping"}, state), do: {[text: "pong"], state}

  # this is for firefox
  @impl true
  def websocket_handle({:ping, _}, state), do: {[text: "pong"], state}

  def websocket_handle({:text, command_json}, state) do
    with {:ok, message_map!} <- Jason.decode(command_json),
         {:ok, message} <- validate(message_map!, state) do
      dispatch(message, state)
    else
      {:error, %Jason.DecodeError{}} ->
        ws_push({:close, 4001, "JSON Parse Failed"}, state)

      # error validating the inner changeset.
      {:error, :validation} ->
        ws_push({:close, 4001, "Invalid Data"}, state)
    end
  end

  def auth_check(%{user_id: nil}), do: {:error, :auth}
  def auth_check(_state), do: :ok

  def process_event(_event, _data, _state) do
    {:error, "Unknown event"}
  end

  def dispatch(message, state) do
    case process_event(message.event, message.data, state) do
      close when elem(close, 0) == :close ->
        ws_push(close, state)

      {:error, err} ->
        wrap_error(err)
        |> prepare_socket_msg(state)
        |> ws_push(state)

      {:error, errors, new_state} ->
        wrap_error(errors)
        |> prepare_socket_msg(new_state)
        |> ws_push(new_state)

      {:noreply, new_state} ->
        ws_push(nil, new_state)

      {:reply, message, new_state} ->
        message
        |> prepare_socket_msg(new_state)
        |> ws_push(new_state)
    end
  end

  defp wrap_error(error) do
    %{
      data: to_map(error),
      event: "error"
    }
  end

  # we expect two three of errors:
  # - Changeset errors
  # - textual errors
  # - anything else
  # the `to_map` function handles all of them

  defp to_map(changeset = %Ecto.Changeset{}) do
    Logger.debug "Changeset errors: " <> inspect(changeset.errors)
    Map.new(changeset.errors, fn {k, {message, _}} -> {k, message} end)
  end

  defp to_map(string) when is_binary(string) do
    %{message: string}
  end

  defp to_map(other) do
    %{message: inspect(other)}
  end

  def prepare_socket_msg(data, state) do
    data
    |> encode_data(state)
    |> prepare_data(state)
  end

  defp encode_data(data, %{encoding: :etf}) do
    data
    |> Map.from_struct()
    |> :erlang.term_to_binary()
  end

  defp encode_data(data, %{encoding: :json}) do
    Jason.encode!(data)
  end

  defp prepare_data(data, %{compression: :zlib}) do
    z = :zlib.open()

    :zlib.deflateInit(z)
    data = :zlib.deflate(z, data, :finish)
    :zlib.deflateEnd(z)

    {:binary, data}
  end

  defp prepare_data(data, %{encoding: :etf}) do
    {:binary, data}
  end

  defp prepare_data(data, %{encoding: :json}) do
    {:text, data}
  end

  def ws_push(frame, state) do
    {List.wrap(frame), state}
  end


  ########################################################################
  # helper functions

  if Mix.env() == :test do
    defp get_callers(request) do
      request_bin = :cowboy_req.header("user-agent", request)

      List.wrap(
        if is_binary(request_bin) do
          request_bin
          |> Base.decode16!()
          |> :erlang.binary_to_term()
        end
      )
    end
  else
    defp get_callers(_), do: []
  end

  # ROUTER

  @impl true
  def websocket_info({:EXIT, _, _}, state), do: exit_impl(state)
  def websocket_info(:exit, state), do: exit_impl(state)
  def websocket_info(:auth_timeout, state), do: auth_timeout_impl(state)
  def websocket_info({:remote_send, message}, state), do: remote_send_impl(message, state)
end
