defmodule Snowchat.WS.WebSocketHelpers do
  alias Riverside.LocalDelivery

  def deliver_channel(channel_id, outgoing_data) do
    deliver({:channel, channel_id}, {:text, Jason.encode!(outgoing_data)})
  end

  def deliver_user(user_id, outgoing_data) do
    deliver({:user, user_id}, {:text, Jason.encode!(outgoing_data)})
  end

  def deliver_session(user_id, session_id, outgoing_data) do
    deliver({:session, user_id, session_id}, {:text, Jason.encode!(outgoing_data)})
  end



  @type destination ::
          {:user, Session.user_id()}
          | {:session, Session.user_id(), String.t()}
          | {:channel, term}

  @spec deliver(destination, {Riverside.Codec.frame_type, any}) :: no_return
  def deliver(destination, msg) do
    LocalDelivery.deliver(destination, msg)
  end
end
