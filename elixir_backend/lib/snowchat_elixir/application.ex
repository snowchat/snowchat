defmodule Snowchat.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  require Logger

  alias Snowchat.State.Supervisors

  @impl true
  def start(_type, _args) do
    http_port = Application.fetch_env!(:snowchat_elixir, :port)
    ws_port = Application.fetch_env!(:snowchat_elixir, :ws_port)

    children = [
      Supervisors.UserSession,
      {
        Plug.Cowboy,
        scheme: :http,
        plug: Snowchat.HTTP.Router,
        options: [
          port: http_port
        ]
      },
      {Riverside, [handler: Snowchat.WS.WebSocketHandler]},
      Snowchat.Postgres.Repo,
      {GenRegistry, worker_module: Snowchat.Guild}
      # {
      #  Redix,
      #  host: System.get_env("REDIS_HOST") || "localhost",
      #  port: String.to_integer(System.get_env("REDIS_PORT") || "6379")
      # }
    ]

    # TODO: if production set machine id dynamically

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Snowchat.Supervisor]

    env = inspect(Application.get_env(:snowchat_elixir, :environment))
    Logger.info("Starting Snowchat Elixir back-end with enviroment: #{env}")
    Logger.info("Starting HTTP server at http://localhost:#{http_port}/")
    Logger.info("Starting WS server at ws://localhost:#{ws_port}/")

    guilds = [
      %{
        id: "420",
        name: "Snowchat Dev",
        members: []
      },
      %{
        id: "421",
        name: "hello world",
        members: []
      },
      %{
        id: "422",
        name: "binrus place",
        members: []
      }
    ]

    {:ok, pid} = Supervisor.start_link(children, opts)

    Elixir.Snowchat.Release.migrate()

    guild_pids =
      Enum.map(guilds, fn guild ->
        {:ok, pid} = GenRegistry.lookup_or_start(Snowchat.Guild, guild.id, [guild])
        Logger.info("Started #{guild.name} with PID #{inspect(pid)}")
        pid
      end)

    IO.inspect(guild_pids)

    {:ok, pid}
  end
end
