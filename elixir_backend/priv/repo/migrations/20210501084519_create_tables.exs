defmodule Snowchat.Postgres.Repo.Migrations.CreateTables do
  use Ecto.Migration

  def change do
    create table(:users, primary_key: false) do
      add :id, :bigint, primary_key: true
      add :username, :text, null: false
      add :password, :text, null: false
      add :display_name, :text, null: false
      add :bio, :text
      add :avatar_url, :text
      add :banner_url, :text
      add :status, :text, default: "online" # can be "online", "idle", "dnd" or "invisible"
      add :custom_status, :text
      add :last_online, :utc_datetime_usec
      add :reason_for_ban, :text
      add :last_ip, :text, null: true
      add :is_developer, :boolean, default: false

      timestamps(type: :utc_datetime_usec)
    end

    create table(:refresh_tokens, primary_key: false) do
      add :id, :bigint, primary_key: true
      add :token, :text, null: false
      add :user_id, references(:users, on_delete: :delete_all, type: :bigint), null: false, primary_key: false

      timestamps(type: :utc_datetime_usec)
    end

    create table(:guilds, primary_key: false) do
      add :id, :bigint, primary_key: true
      add :display_name, :text, null: false
      add :icon_url, :text, null: false
      add :banner_url, :text, null: false
      add :bio, :text, null: false
      add :owner_id, references(:users, on_delete: :delete_all, type: :bigint), null: false, primary_key: false

      timestamps(type: :utc_datetime_usec)
    end

    create table(:channels, primary_key: false) do
      add :id, :bigint, primary_key: true
      add :channel_name, :text, null: false
      add :topic, :text, null: false
      add :channel_type, :text, null: false
      add :last_message_id, :bigint
      add :guild_id, references(:guilds, on_delete: :delete_all, type: :bigint), null: false, primary_key: false

      timestamps(type: :utc_datetime_usec)
    end

    create table(:messages, primary_key: false) do
      add :id, :bigint, primary_key: true
      add :content, :text, null: false
      add :message_type, :text, null: false
      add :channel_id, references(:channels, on_delete: :delete_all, type: :bigint), null: false, primary_key: false
      add :author_id, references(:users, on_delete: :delete_all, type: :bigint), null: false, primary_key: false

      timestamps(type: :utc_datetime_usec)
    end
  end
end
