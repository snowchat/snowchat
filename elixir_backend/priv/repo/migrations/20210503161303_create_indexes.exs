defmodule Snowchat.Postgres.Repo.Migrations.CreateIndexes do
  use Ecto.Migration

  def change do
    create unique_index(:users, [:username])
  end
end
