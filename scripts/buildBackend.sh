#!/bin/bash
set -e # Exit if an error occurs
echo "Building Snowchat Backend..."
echo ""

if command -v npm &>/dev/null ; then
    echo 'npm exists'
fi
if command -v pnpm &>/dev/null ; then
    echo 'pnpm exists'
fi
if command -v node &>/dev/null ; then
    echo 'node exists'
fi
if command -v deno &>/dev/null ; then
    echo 'deno exists'
fi

cd util
pnpm run build

cd ../gateway
pnpm run build

cd ../http-api
pnpm run build

cd ../bundle
pnpm run build
