Write-Host "Building Snowchat Backend..."
Write-Host ""

Set-Location util
pnpm run build
if(!($?)) {
    throw 'Error'
}

Set-Location ../gateway
pnpm run build
if(!($?)) {
    throw 'Error'
}

Set-Location ../http-api
pnpm run build
if(!($?)) {
    throw 'Error'
}

Set-Location ../bundle
pnpm run build
if(!($?)) {
    throw 'Error'
}
