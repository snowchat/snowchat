#!/bin/bash
set -e

function _createTerminal() {
    xdotool key ctrl+shift+p
    xdotool type 'terminal: create terminal'
    xdotool key Return
}

function _splitTerminal() {
    xdotool key ctrl+shift+p
    xdotool type 'terminal: split terminal'
    xdotool key Return
}

function _setIcon() {
    icon="$1"
    press_down="$2"
    xdotool key ctrl+shift+p
    xdotool type 'terminal: change icon'
    xdotool key Return
    xdotool type $icon
    if [[ $press_down = "true" ]]; then
        xdotool key Down
        sleep .5
    fi
    xdotool key Return
}

# Gateway
_createTerminal
sleep .5
_setIcon 'megaphone'
xdotool type 'cd '${1:-.}'/gateway ; clear' ; xdotool key Return
sleep 1

# API
_splitTerminal
sleep .5
_setIcon 'rocket'
xdotool type 'cd '${1:-.}'/http-api ; clear' ; xdotool key Return
sleep 1
