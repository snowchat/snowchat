Write-Host "Prettifying Snowchat Code..."
Write-Host ""

Set-Location util
pnpm run prettify
if(!($?)) {
    throw 'Error'
}

Set-Location ../gateway
pnpm run prettify
if(!($?)) {
    throw 'Error'
}

Set-Location ../http-api
pnpm run prettify
if(!($?)) {
    throw 'Error'
}

Set-Location ../bundle
pnpm run prettify
if(!($?)) {
    throw 'Error'
}

Set-Location ../react-frontend
pnpm run prettify
if(!($?)) {
    throw 'Error'
}
