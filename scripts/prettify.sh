#!/bin/bash
set -e # Exit if an error occurs
echo "Prettifying Snowchat Code..."
echo ""

cd util
pnpm run prettify

cd ../gateway
pnpm run prettify

cd ../http-api
pnpm run prettify

cd ../bundle
pnpm run prettify

cd ../react-frontend
pnpm run prettify
