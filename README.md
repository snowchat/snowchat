<div align="center">
  <img width="500" src="https://gitlab.com/snowchat/snowchat/-/raw/master/logos/logo-wordmark-brand.svg" />
</div>
<br/>

[![forthebadge](https://forthebadge.com/images/badges/check-it-out.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/not-a-bug-a-feature.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/open-source.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/made-with-javascript.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/it-works-why.svg)](https://forthebadge.com)
[![Discord](https://img.shields.io/discord/835919990401269775?color=7489d5&logo=discord&logoColor=ffffff")](https://discord.gg/gmMNzdyvM7)
[![Status](https://img.shields.io/static/v1?label=Status&message=Development&color=blue)](https://gitlab.com/snowchat/snowchat)
[![Lines of code](https://img.shields.io/tokei/lines/gitlab/snowchat/snowchat)](https://gitlab.com/snowchat/snowchat)
[![Donate Monero](https://img.shields.io/static/v1?label=Donate&message=Monero&color=7489d5&logo=monero&logoColor=ffffff")](#donate)

> Imagine a place that is actually yours and you have full control over it.

It is similar to Slack, Rocket.chat, JetBrains Space and Discord.

As the famous Ben Awad would say:
> To the moon 🚀!

## Features

- Open Source
- Selfhostable
- Customizable
- Secure

## Donate
Monero: 4AycwShKZnzYpBk6GxbLQLBeQ1PnqG94Dcgsm5Nsbbn7Z7uyJsdmKpuZ7MKwqiMQwrZfR7aaMK3Bjd8wLy1MTNSTG6y6xNf

## Components

This repository contains:

- [React Frontend](https://gitlab.com/snowchat/snowchat/tree/master/react-frontend)
- [HTTP API Server](https://gitlab.com/snowchat/snowchat/tree/master/http-api)
- [WebSocket Gateway Server](https://gitlab.com/snowchat/snowchat/tree/master/gateway)
- [Utility and Database Models](https://gitlab.com/snowchat/snowchat/tree/master/util)
<!--
- [HTTP CDN Server](https://gitlab.com/snowchat/snowchat/tree/master/cdn)
- [WebRTC Server](https://gitlab.com/snowchat/snowchat/tree/master/webrtc)
-->

## NOTE: Snowchat does not have official support for operating systems other than Linux.
The guides on this page are designed for Linux and the commands may not be available on other operating systems.

## Dependencies:
- Production setup: [Node.js](https://nodejs.dev), [PNPM](https://pnpm.io/installation) (You can use NPM to install it), [Git](https://git-scm.com/)
- Production runtime: Docker, Docker Compose
- Development: Docker, Docker Compose, [Node.js](https://nodejs.dev), [PNPM](https://pnpm.io/installation) (You can use NPM to install it), [Git](https://git-scm.com/)

## Production Usage:
### Use a TLS/HTTPS Certificate (Required):
Guide at https://gitlab.com/snowchat/snowchat#using-certificates
### Setup:
```sh
# Clone the repository
git clone https://gitlab.com/snowchat/snowchat
cd snowchat
# Build React front-end
cd react-frontend
pnpm i
pnpm run build
cd ..
```
### Actual usage
```sh
# Run backend servers, YugabyteDB, Redis and Caddy
docker-compose -f prod.yml up
```

## Development Usage:
### Use a TLS/HTTPS Certificate (Required):
Guide at https://gitlab.com/snowchat/snowchat#using-certificates
### Setup:
```sh
# Clone the repository
git clone https://gitlab.com/snowchat/snowchat
cd snowchat
# Install dependencies
cd react-frontend
pnpm i
cd ../util
pnpm i
cd ../http-api
pnpm i
cd ../gateway
pnpm i
cd ../bundle
pnpm i
```
### Actual usage
```sh
# Start React dev enviroment
cd react-frontend
pnpm run dev

# Run YugabyteDB, Redis and Caddy
# (Run this in other terminal!)
docker-compose -f dev.yml up

# (Run this in other terminal!)
# Run HTTP API
cd http-api
pnpm run dev
# (Run this in other terminal!)
# Run WebSocket Gateway
cd gateway
pnpm run dev
# (Run this in other terminal!)
# Watch for util changes
cd util
pnpm run watch
```

## Using Certificates:
### Option A: Generating a Self-Signed Certificate For Use:
https://github.com/FiloSottile/mkcert#installation

```sh
mkdir certs
cd certs
mkcert localhost 127.0.0.1 ::1
mv localhost+2.pem certificate.pem
mv localhost+2-key.pem certificate-key.pem
```
### Option B: Using Your Own Certificates:
Create a `certs` directory if not already created.<br/>
Place your private key PEM file to the `certs` directory with the name `certificate-key.pem`.<br/>
Place your certificate PEM file to the `certs` directory with the name `certificate.pem`.<br/>

## Docker Info
If you are manually compiling the Docker images please run them with the context of the project directory (The same this README is located in) and the Dokcerfile of the project you want to compile!
Example command:
```bash
cd http-api
docker build .. -f Dockerfile
```
