# Enviroment variables:
## Global
### `DATABASE_URL`
Defaults to "postgres://snowy:example@localhost:5433/snowchat"
### ACCESS_TOKEN_SECRET
Configures the access JWT secret<br/>
Defaults to "secret"
### REFRESH_TOKEN_SECRET
Configures the refresh JWT secret<br/>
Defaults to "secret"

## Bundle
### `PORT`
Configures the port for the server<br/>
Defaults to "4000"
## HTTP API
### `PORT`
Configures the port for the server<br/>
Defaults to "4000"
## WebSocket Gateway
### `PORT`
Configures the port for the server<br/>
Defaults to "4001"
