import React from 'react';

export const WebSocketContext = React.createContext(null);

export const useConn = () => {
  return React.useContext(WebSocketContext);
};
