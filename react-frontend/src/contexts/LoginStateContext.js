import React from 'react';

export const LoginStateContext = React.createContext({});

export const useLoginStateContext = () => React.useContext(LoginStateContext);
