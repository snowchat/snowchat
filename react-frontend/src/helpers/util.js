class Utilities {
  constructor() {}
  errorCodeGenerator(err = '') {
    let value = 0;
    for (const c of [...err]) value += c.charCodeAt(0);
    return value;
  }
  getOperatingSystem() {
    let operatingSystem = 'Not known';
    if (navigator.userAgent.indexOf('Win') !== -1) {
      operatingSystem = 'Windows';
    }
    if (navigator.userAgent.indexOf('Mac') !== -1) {
      operatingSystem = 'MacOS';
    }
    if (navigator.userAgent.indexOf('Linux') !== -1) {
      operatingSystem = 'Linux';
    }
    if (navigator.userAgent.indexOf('Android') !== -1) {
      operatingSystem = 'Android';
    }
    if (navigator.userAgent.indexOf('like Mac') !== -1) {
      operatingSystem = 'iOS';
    }

    return operatingSystem;
  }
}

export default new Utilities();
