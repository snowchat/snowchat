/*
 * This file extends
 * Reconnecting WebSocket
 * by Pedro Ladaria <pedro.ladaria@gmail.com>
 * https://github.com/pladaria/reconnecting-websocket
 * License MIT
 *
 * Edited by ThatOneLukas to
 * include custom logging
 */

import cconsole from '@helpers/cconsole';
import ReconnectingWebSocket from 'reconnecting-websocket';

const getGlobalWebSocket = () => {
  if (typeof WebSocket !== 'undefined') {
    return WebSocket;
  }
};

const isWebSocket = (w) => typeof w !== 'undefined' && !!w && w.CLOSING === 2;

const DEFAULT = {
  maxReconnectionDelay: 10000,
  minReconnectionDelay: 1000 + Math.random() * 4000,
  minUptime: 5000,
  reconnectionDelayGrowFactor: 1.3,
  connectionTimeout: 4000,
  maxRetries: Infinity,
  maxEnqueuedMessages: Infinity,
  startClosed: false,
  debug: false,
};

export default class ReconnectingWebSocket2 extends ReconnectingWebSocket {
  _connect() {
    if (this._connectLock || !this._shouldReconnect) {
      return;
    }
    this._connectLock = true;

    const {
      maxRetries = DEFAULT.maxRetries,
      connectionTimeout = DEFAULT.connectionTimeout,
      WebSocket = getGlobalWebSocket(),
    } = this._options;

    if (this._retryCount >= maxRetries) {
      cconsole.warnc('ReconnectingWebSocket', 'Max retries reached:', this._retryCount, '>=', maxRetries);
      return;
    }

    this._retryCount++;

    this._removeListeners();
    if (!isWebSocket(WebSocket)) {
      throw Error('No valid WebSocket class provided');
    }
    this._wait()
      .then(() => this._getNextUrl(this._url))
      .then((url) => {
        // close could be called before creating the ws
        if (this._closeCalled) {
          return;
        }
        cconsole.logc('ReconnectingWebSocket', this._retryCount > 0 ? 'Reconnecting' : 'Connecting', {
          url,
          protocols: this._protocols,
          retries: this._retryCount,
        });
        this._ws = this._protocols ? new WebSocket(url, this._protocols) : new WebSocket(url);
        this._ws.binaryType = this._binaryType;
        this._connectLock = false;
        this._addListeners();

        this._connectTimeout = setTimeout(() => this._handleTimeout(), connectionTimeout);
      });
  }
}
