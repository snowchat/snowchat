import config from '@config';

import cconsole from '@helpers/cconsole';
import ReconnectingWebSocket from '@helpers/reconnectingWebSocket';

/**
 * Log out callback
 *
 * @callback logoutCallback
 */

export /**
 * Connect to the API
 *
 * @param {string} token Access Token
 * @param {logoutCallback} logout Log out callback
 * @param {string} [url=config.gateway_url] WebSocket URL
 * @param {number} [connectionTimeout=config.connection_timeout] Connection Timeout
 * @param {number} [heartbeatInterval=config.heartbeat_interval] Heartbeat Interval
 * @returns {Promise<{close, addListener, once, oncePromise, send}>} Promise which should contain the connection details and helpers
 */
const connect = (
  token,
  logout,
  url = config.gateway_url,
  connectionTimeout = config.connection_timeout,
  heartbeatInterval = config.heartbeat_interval,
) =>
  new Promise((resolve, reject) => {
    const protocol = ['snowchat', token];
    const wsOptions = { connectionTimeout };
    cconsole.infoc('Raw WebSocket', 'Connecting with access token:', true ? '<redacted>' : token);
    const socket = new ReconnectingWebSocket(url, protocol, wsOptions);

    const apiSend = (event, data) => {
      // Commented because ReconnectingWebSocket has a queue system
      /*if (socket.readyState !== socket.OPEN) {
        return;
      }/**/

      const message = { event, data };

      const messageString = JSON.stringify(message);

      event !== 'ping' && cconsole.logc('Raw WebSocket', 'Sending', message, messageString);

      socket.send(messageString);
    };

    const listeners = [];

    socket.addEventListener('close', (e) => {
      cconsole.errorc('Raw WebSocket', 'Closed WebSocket', e);
      if (e.code === 4000) {
        // Invalid Token
        socket.close();
        logout();
      }
      reject(e);
    });

    socket.addEventListener('error', (e) => {
      cconsole.errorc('Raw WebSocket', 'WebSocket Error', e);
    });

    socket.addEventListener('open', () => {
      cconsole.logc('Raw WebSocket', 'Connected', socket);

      const id = setInterval(() => {
        if (socket.readyState === socket.CLOSED) {
          clearInterval(id);
        } else {
          config.log_heartbeat && cconsole.logc('Raw WebSocket', 'ping');
          apiSend('ping', null);
        }
      }, heartbeatInterval);
    });

    socket.addEventListener('message', (e) => {
      var message = JSON.parse(e.data);

      if (message.event === 'pong') {
        return config.log_heartbeat && cconsole.logc('Raw WebSocket', 'Got Pong event');
      }

      cconsole.logc('Raw WebSocket', 'Got message:', message);

      listeners.filter(({ event }) => event === message.event).forEach((listener) => listener.handler(message.data));
    });

    const connection = {
      close: () => socket && socket.readyState !== socket.CLOSED && socket.close(),
      get isClosed() {
        return !socket || socket.readyState === socket.CLOSED;
      },
      get isConnected() {
        return socket && socket.readyState === socket.OPEN;
      },
      addListener: (event, handler) => {
        const listener = { event, handler };

        listeners.push(listener);

        return () => listeners.splice(listeners.indexOf(listener), 1);
      },
      once: (event, handler) => {
        cconsole.logc('Raw WebSocket', 'Registered one time only', event, 'event');
        const listener = { event, handler };

        listener.handler = (...args) => {
          handler(...args);
          listeners.splice(listeners.indexOf(listener), 1);
        };

        listeners.push(listener);
      },
      oncePromise(event) {
        return new Promise(
          function (resolve, reject) {
            var timeoutId = setTimeout(reject, 5000 || timeout, 'Timeout Error');
            this.once(event, (message) => {
              clearTimeout(timeoutId);
              cconsole.logc('Raw WebSocket', 'oncePromise', event, message);
              resolve(message);
            });
          }.bind(this),
        );
      },
      send: apiSend,
    };
    resolve(connection);
  });
