class CustomConsole {
  showTrace = false;
  constructor() {}
  custom(name, consoleFn, ...data) {
    this.showTrace && (consoleFn === 'info' || consoleFn === 'log' || consoleFn === 'debug') && (consoleFn = 'trace');
    console[consoleFn](`%c[${name}]`, 'color: #800080;font-weight: bold', ...data);
  }
  info(...args) {
    this.custom('Info', 'info', ...args);
  }
  log(...args) {
    this.custom('Log', 'log', ...args);
  }
  debug(...args) {
    this.custom('Debug', 'debug', ...args);
  }
  error(...args) {
    this.custom('Error', 'error', ...args);
  }
  warn(...args) {
    this.custom('Warning', 'warn', ...args);
  }

  infoc(name, ...args) {
    this.custom(name, 'info', ...args);
  }
  logc(name, ...args) {
    this.custom(name, 'log', ...args);
  }
  debugc(name, ...args) {
    this.custom(name, 'debug', ...args);
  }
  errorc(name, ...args) {
    this.custom(name, 'error', ...args);
  }
  warnc(name, ...args) {
    this.custom(name, 'warn', ...args);
  }
}

export default new CustomConsole();
