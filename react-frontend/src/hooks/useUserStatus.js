import { useEffect, useState } from 'react';

import { useConn } from '@contexts/WebSocketContext';
import axios from 'axios';

export default function useUserStatus() {
  const conn = useConn();
  const [status, setStatusInner] = useState('online');

  useEffect(() => {
    // on mount
    const unsubscribe = conn.addListener('ON_STATUS_CHANGE_SELF', (data) => {
      setStatusInner(data);
    });
    return () => {
      // on unmount
      unsubscribe();
    };
  }, []);

  const setStatus = (status) => {
    axios.post(`/users/@me/set_status`, {
      status: status,
    });
    setStatusInner(status);
  };

  return [status, setStatus];
}
