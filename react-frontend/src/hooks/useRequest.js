import { useState } from 'react';
import axios from 'axios';

import cconsole from '@helpers/cconsole';

const useRequest = (isJson = false) => {
  const [status, setStatus] = useState('idle');
  const [content, setContent] = useState(null);
  const [error, setError] = useState(null);

  const fetchRequest = async (url) => {
    setStatus('loading');

    try {
      await ((ms) => new Promise((resolve) => setTimeout(resolve, ms)))(500);
      /*const res = await fetch(url);

      if (!res.ok)
        throw Error(`${res.status}: ${res.statusText} (URL: ${res.url})`);

      if (isJson) setContent(await res.json());
      else setContent(await res.text());
      setStatus('success');*/
      const res = await axios.get(url);

      cconsole.debugc('useRequest', 'Response got', res);

      setContent(res.data);

      setStatus('success');
    } catch (error) {
      cconsole.errorc('useRequest', 'Error while sending request', error);
      setError(error);
      setStatus('error');
      return;
    }
  };

  return { status, content, error, fetchRequest };
};

export default useRequest;
