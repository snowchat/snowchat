import { useState } from 'react';

export function useToken() {
  const [token, setTokenInternal] = useState(localStorage.getItem('token'));

  function setToken(token) {
    token == null ? localStorage.removeItem('token') : localStorage.setItem('token', token);
    setTokenInternal(token);
  }

  return [token, setToken];
}
