import React from 'react';
import './Sidebar.css';

import Button from '@components/blocks/Button/Button';

import { RiLogoutCircleRLine, RiUser3Line } from 'react-icons/ri';
import { useLoginStateContext } from '@contexts/LoginStateContext';

function Sidebar(props) {
  const { logout } = useLoginStateContext();
  return (
    <div className="Sidebar">
      <div className="sidebar-user">
        <RiUser3Line className="sidebar-user-icon" />
        Admin
        <Button onClick={logout}>
          <RiLogoutCircleRLine />
          Log Outa
        </Button>
      </div>
    </div>
  );
}

export default Sidebar;
