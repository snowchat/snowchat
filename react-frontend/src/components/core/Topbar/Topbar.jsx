import React, { useState } from 'react';
import { useDebouncedCallback } from 'use-debounce';
import './Topbar.css';
import logo from '@assets/logos/logo.svg';
import { RiSearchLine } from 'react-icons/ri';

function Topbar(props) {
  return (
    <header className="Topbar">
      <img
        src={logo}
        alt="Snowchat Logo"
        style={{ height: '2rem' }}
        onClick={(e) => {
          props.setSidebarCollapsed(!props.sidebarCollapsed);
        }}
      />
      <Searchbar />
    </header>
  );
}

const data = [
  {
    type: 'User',
    content: 'Binru',
  },
  {
    type: 'Group',
    content: 'Snowchat Development',
  },
  {
    type: 'Group',
    content: 'binrus place',
  },
  {
    type: 'Channel',
    content: '#general',
  },
  {
    type: 'Channel',
    content: '#dev',
  },
  {
    type: 'Channel',
    content: '#dev',
  },
];

const useSearchResults = () => {
  const [status, setStatus] = useState('idle');
  const [results, setResults] = useState(null);
  const [error, setError] = useState(null);

  const search = async (term) => {
    term = term.trim().toLowerCase();
    setStatus('loading');

    // check for term being empty
    if (term === '') {
      setResults(null);
      setStatus('termempty');
      return;
    }

    // TODO: request here
    await ((ms) => new Promise((resolve) => setTimeout(resolve, ms)))(500);
    const filteredData = data.filter((v) => {
      var c = v.content.toLowerCase();
      return c.includes(term);
    });
    const error = null;
    const noresults = filteredData == null || filteredData.length === 0;

    if (noresults) {
      setResults(null);
      setStatus('noresults');
      return;
    }

    if (error) {
      setError(error);
      setStatus('error');
      return;
    }
    setResults(filteredData);
    setStatus('success');
  };

  return { status, results, error, search };
};

function Searchbar(props) {
  const [isFocused, setIsFocused] = useState(false);
  const { status, results, error, search } = useSearchResults();

  const changeHandler = (e) => {
    const term = e.target.value;

    search(term);
  };

  const debouncedChangeHandler = useDebouncedCallback(
    // function
    changeHandler,
    // delay in ms
    1000,
  );

  const focusHandler = () => isFocused || setIsFocused(true);
  const blurHandler = () => isFocused && setIsFocused(false);

  const debugStatus = false;

  const getSearchResultsJSX = () => (
    <>
      {debugStatus && (
        <SearchResult type={'status'} key={-1}>
          {status}
        </SearchResult>
      )}
      {status === 'loading' && <div className="search-loading">Loading...</div>}
      {status === 'noresults' && <div className="search-no-results">No results found :(</div>}
      {status === 'error' && <div className="search-error">There was an error: {error}</div>}
      {status === 'success' &&
        results &&
        results.map((result, i) => {
          return (
            <SearchResult type={result.type} key={i}>
              {result.content}
            </SearchResult>
          );
        })}
    </>
  );

  return (
    <div className="searchbar">
      <div className="searchbar-input">
        <div className="searchbar-icon">
          <RiSearchLine />
        </div>
        <input
          type="text"
          className="searchbar-inner truncate"
          id="searchbar-input"
          placeholder="Search for guilds, text channels, or users"
          onFocus={focusHandler}
          onBlur={blurHandler}
          onChange={changeHandler}
        />
      </div>
      {isFocused && <SearchOverlay className={'status-' + status}>{getSearchResultsJSX()}</SearchOverlay>}
    </div>
  );
}

function SearchOverlay({ children, className = '', contentClassName = '', scrollerClassName = '' }) {
  return (
    <div className={`SearchOverlay ${className}`}>
      <div className={`SearchOverlay-scroller ${scrollerClassName}`}>
        <div className={`SearchOverlay-content ${contentClassName}`}>{children}</div>
      </div>
    </div>
  );
}

function SearchResult({ children, type }) {
  return (
    <div className="SearchResult">
      {children} <div className="SearchResult-type">{type}</div>
    </div>
  );
}

export default Topbar;
