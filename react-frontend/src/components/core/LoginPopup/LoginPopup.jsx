import React from 'react';
import './LoginPopup.css';

import { RiGitlabLine, RiDiscordFill } from 'react-icons/ri';

const NavBar = React.lazy(() => import('@components/core/NavBar/NavBar'));

function LoginPopup({ children }) {
  return (
    <div className="LoginPopup">
      <NavBar />
      <div className="LoginPopup-popup">{children}</div>
      <footer className="footer">
        <a href="https://gitlab.com/snowchat/snowchat/-/issues">Report a bug</a>
        <div className="footer-logos">
          <a href="https://gitlab.com/snowchat">
            <RiGitlabLine className="gitlab-logo" />
          </a>
          <a href="https://discord.gg/gmMNzdyvM7">
            <RiDiscordFill className="discord-logo" />
          </a>
        </div>
      </footer>
    </div>
  );
}

export default LoginPopup;
