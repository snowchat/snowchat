import React, { useState, useEffect } from 'react';
import './BaseLayout.css';
import './userarea.css';
import { useLoginStateContext } from '@contexts/LoginStateContext';
import { RiMicFill, RiMicOffFill, RiVolumeUpFill, RiVolumeMuteFill, RiSettings5Fill } from 'react-icons/ri';
import { FaCircle, FaMoon, FaMinusCircle, FaDotCircle } from 'react-icons/fa';
import { useConn } from '@contexts/WebSocketContext';
import useToggle from '@hooks/useToggle';
import Separator from '@components/blocks/Separator/Separator';
import axios from 'axios';

import Foco from 'react-foco';
import Pfp from '@components/blocks/Pfp/Pfp';
import GuildList from '../GuildList/GuildList';
import Tooltip from '@components/blocks/Tooltip/Tooltip';
import useUserStatus from '@hooks/useUserStatus';
import ChatView from '@components/core/ChatView/ChatView';
import { useParams } from 'react-router';
import { LinkButton } from '@components/blocks/Button/Button';

export default function BaseLayout() {
  return (
    <div className="BaseLayout">
      <GuildList />
      <Content
        sidebar={
          <>
            <ChannelList />
          </>
        }
        maincontent={<ChatView />}
      />
      {/*<h2>App</h2>
       */}
    </div>
  );
}

function Content({ sidebar, maincontent }) {
  return (
    <div className="Content">
      <Sidebar>{sidebar}</Sidebar>
      <div className="MainContent">{maincontent}</div>
    </div>
  );
}

function Sidebar({ children }) {
  const [muted, toggleMute] = useToggle();
  const [deafened, toggleDeafen] = useToggle();
  function openSettings() {}

  // TODO: create global store for muted and deafened
  // TODO: create settings menu
  return (
    <div className="Sidebar">
      <div className="Sidebar-custom-content">{children}</div>
      <div className="Sidebar-userarea-wrapper">
        <div className="Sidebar-userarea">
          <StatusChooser />
          <div className="Sidebar-userarea-username">Username</div>
          <div className="Sidebar-userarea-buttons">
            <Tooltip tooltip="test">
              <div className="Userarea-mute-btn Userarea-btn" role="button" onClick={toggleMute}>
                {muted ? <RiMicOffFill /> : <RiMicFill />}
              </div>
            </Tooltip>
            <Tooltip tooltip="test">
              <div className="Userarea-deafen-btn Userarea-btn" role="button" onClick={toggleDeafen}>
                {deafened ? <RiVolumeMuteFill /> : <RiVolumeUpFill />}
              </div>
            </Tooltip>
            <Tooltip tooltip="test">
              <div className="Userarea-settings-btn Userarea-btn" role="button" onClick={openSettings}>
                <RiSettings5Fill />
              </div>
            </Tooltip>
          </div>
        </div>
      </div>
    </div>
  );
}

function StatusChooserItem({ color, icon, text, id, ...props }) {
  return (
    <div className="StatusChooserItem" id={`StatusChooser-${id}`} role="menuitem" {...props}>
      <div className="StatusChooserItem-icon" style={{ color }}>
        {icon}
      </div>
      <div className="StatusChooserItem-text">{text}</div>
    </div>
  );
}

function StatusChooserItemCustom({ text, ...props }) {
  return (
    <div className="StatusChooserItem" id="StatusChooser-custom-status" role="menuitem" {...props}>
      <div className="StatusChooserItem-icon">{/*maybe emoji here?*/}</div>
      <div className="StatusChooserItem-text">{text}</div>
    </div>
  );
}

function StatusChooser() {
  const [statusChangerOpen, setStatusChangerOpen] = useState(false);

  const [status, setStatus] = useUserStatus();
  const { logout } = useLoginStateContext();

  return (
    <div className="Sidebar-userarea-avatar-wrapper">
      <Foco onClickOutside={() => setStatusChangerOpen(false)}>
        <Pfp
          src="url('https://images.unsplash.com/photo-1623889679077-eb97bac494ad?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80')"
          status={status}
          aria-label="Set Status"
          role="button"
          onClick={() => setStatusChangerOpen(!statusChangerOpen)}
        />
        {statusChangerOpen && (
          <div className="StatusChooser">
            <StatusChooserItem
              color="rgb(59, 165, 93)"
              icon={<FaCircle />}
              text="Online"
              id="online"
              onClick={() => {
                setStatusChangerOpen(false);
                setStatus('online');
              }}
            />
            <Separator />
            <StatusChooserItem
              color="rgb(250, 168, 26)"
              icon={<FaMoon />}
              text="Idle"
              id="idle"
              onClick={() => {
                setStatusChangerOpen(false);
                setStatus('idle');
              }}
            />
            <StatusChooserItem
              color="rgb(237, 66, 69)"
              icon={<FaMinusCircle />}
              text="Do Not Disturb"
              id="dnd"
              onClick={() => {
                setStatusChangerOpen(false);
                setStatus('dnd');
              }}
            />
            <StatusChooserItem
              color="rgb(116, 127, 141)"
              icon={<FaDotCircle />}
              text="Invisible"
              id="invisible"
              onClick={() => {
                setStatusChangerOpen(false);
                setStatus('invisible');
              }}
            />
            <Separator />
            <StatusChooserItemCustom text="Edit Custom Status" />
            <button className="Button" onClick={logout}>
              Log Out
            </button>
          </div>
        )}
      </Foco>
    </div>
  );
}

function useChannels(guildId) {
  const [channels, setChannels] = useState([]);
  const conn = useConn();
  useEffect(() => {
    axios.get(`/guilds/${guildId}/channels`).then((res) => setChannels(res.data));
    const unsubscribe1 = conn.addListener('CHANNEL_CREATE', (channel) => {
      // TODO: fix channels not showing up/replacing others when created
      channels.push(channel);
      setChannels(channels);
    });
    const unsubscribe2 = conn.addListener('CHANNEL_DELETE', (channel) => {
      setChannels(channels.filter((c) => c.id !== channel.id));
    });
    return () => {
      unsubscribe1();
      unsubscribe2();
    };
  }, []);

  return channels;
}

function ChannelList() {
  const { guildId, channelId, ...params } = useParams();
  const channels = useChannels(guildId);
  return (
    <ul>
      {channels.map(channel => (
        <li key={channel.id}>
          <LinkButton to={`/app/channels/${guildId}/${channel.id}`}>#{channel.name}</LinkButton>
        </li>
      ))}
    </ul>
  );
}
