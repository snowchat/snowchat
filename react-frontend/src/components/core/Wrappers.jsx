import React from 'react';

import _HotKeys from 'react-hotkeys';
const { GlobalHotKeys, getApplicationKeyMap, recordKeyCombination } = _HotKeys;

import Modal from '@components/blocks/Modal/Modal';
import Button from '@components/blocks/Button/Button';

import useToggle from '@hooks/useToggle';

function Wrappers({ children }) {
  return <HotKeyWrapper>{children}</HotKeyWrapper>;
}

function HotKeyWrapper({ children }) {
  const hotkeyMap = {
    SEARCH: {
      name: 'Search',
      sequences: ['ctrl+f', 'ctrl+k'],
    },
    SAVE_CHANGES: {
      name: 'Save changes (ex: editing role)',
      sequences: 'ctrl+s',
    },
    KONAMI_CODE: {
      name: 'KONAMI CODE!!!!',
      sequences: 'up up down down left right left right b a enter',
    },
    SHOW_SHORTCUTS: {
      name: 'Show shortcuts',
      sequences: 'ctrl+1',
    },
  };

  const [konami, toggleKonami] = useToggle();
  const [showKeys, toggleShowKeys] = useToggle();
  const hotkeyHandlers = {
    SEARCH: (e) => {
      e.preventDefault();
      //document.getElementById('searchbar-input').focus();
    },
    KONAMI_CODE: (e) => {
      toggleKonami();
    },
    SHOW_SHORTCUTS: (e) => {
      toggleShowKeys();
    },
  };

  return (
    <GlobalHotKeys keyMap={hotkeyMap} handlers={hotkeyHandlers}>
      <Modal
        isOpen={konami}
        onclose={toggleKonami}
        rightButton={
          <Button brandcolor onClick={toggleKonami}>
            Enable
          </Button>
        }
        leftButton={
          <Button secondary onClick={toggleKonami}>
            Cancel
          </Button>
        }
      >
        <h2 style={{ margin: '0px' }}>KONAMI CODE!!</h2>
      </Modal>
      <Modal isOpen={showKeys} onclose={toggleShowKeys} leftButton={<Button onClick={toggleShowKeys}>Okay</Button>}>
        {/*<pre>{JSON.stringify(getApplicationKeyMap(), null, 2)}</pre>*/}
        <KeymapModalContent />
      </Modal>
      {children}
    </GlobalHotKeys>
  );
}

function KeymapModalContent() {
  const keyMap = getApplicationKeyMap();

  return (
    <table>
      <tbody>
        {Object.keys(keyMap).reduce((memo, actionName) => {
          const { sequences, name } = keyMap[actionName];

          memo.push(
            <tr key={name || actionName}>
              <td
                style={{
                  padding: '0.5rem',
                  backgroundColor: 'var(--color-bg)',
                }}
              >
                {name}
              </td>
              <td
                style={{
                  padding: '0.5rem',
                  backgroundColor: 'var(--color-bg)',
                }}
              >
                <div
                  style={{
                    display: 'flex',
                    height: '100%',
                    gap: '0.25rem',
                  }}
                >
                  {sequences.map(({ sequence }) => (
                    <span style={{}} key={sequence}>
                      {sequence}
                    </span>
                  ))}
                </div>
              </td>
            </tr>,
          );

          return memo;
        }, [])}
      </tbody>
    </table>
  );
}

export default Wrappers;
