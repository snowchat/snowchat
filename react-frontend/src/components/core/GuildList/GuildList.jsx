import React, { useState, useEffect } from 'react';
import './GuildList.css';
import Separator from '@components/blocks/Separator/Separator';
import { useHistory, useParams } from 'react-router';
import useFitText from 'use-fit-text';
import Tooltip from '@components/blocks/Tooltip/Tooltip';
import axios from 'axios';
import Modal from '@components/blocks/Modal/Modal';
import Button from '@components/blocks/Button/Button';

import { RiAddFill } from 'react-icons/ri';
import { HtmlField } from '@components/blocks/Field/Field';
import { InfoModal } from '@components/blocks/Modal/Modal';
import { useConn } from '@contexts/WebSocketContext';

function GuildListSeparator() {
  return (
    <div className="GuildListSeparator-wrapper">
      <div className="GuildListSeparator">
        <Separator thickness="3px" />
      </div>
    </div>
  );
}

const acronymGenerator = (name) =>
  name
    .split(' ')
    .map((v) => v.charAt(0))
    .join('');
const acronymGenerator2 = (str) => {
  if (str)
    return str
      .replace(/'s /g, ' ')
      .replace(/\w+/g, function (e) {
        return e[0];
      })
      .replace(/\s/g, '');
};

function TextAcronym({ acronym }) {
  const { fontSize, ref } = useFitText();

  return (
    <div
      ref={ref}
      style={{ fontSize, width: '100%', height: '100%' }}
      className="GuildListGuild GuildListGuild-acronym"
    >
      {acronym}
    </div>
  );
}

function GuildListGuild({ guild }) {
  const params = useParams();
  const history = useHistory();

  const { id, icon_url, name, channel } = guild;

  return (
    <GuildListItem
      active={params.guildId === id.toString()}
      onClick={() => history.push(`/app/channels/${id}/${channel}`)}
      tooltip={name}
      className={icon_url ? 'disable-guildlist-item-color' : ''}
      childProps={
        icon_url
          ? {
              style: {
                backgroundImage: `url(${icon_url})`,
              },
            }
          : {}
      }
    >
      {icon_url ? <div className="GuildListGuild"></div> : <TextAcronym acronym={acronymGenerator2(name)} />}
    </GuildListItem>
  );
}

function useGuilds() {
  const [guilds, setGuilds] = useState([]);
  const conn = useConn();
  useEffect(() => {
    axios.get('/users/@me/guilds').then((res) => setGuilds(res.data));
    const unsubscribe1 = conn.addListener('GUILD_CREATE', (guild) => {
      // TODO: fix guilds not showing up/replacing others when created
      guilds.push(guild);
      setGuilds(guilds);
    });
    const unsubscribe2 = conn.addListener('GUILD_DELETE', (guild) => {
      setGuilds(guilds.filter((g) => g.id !== guild.id));
    });
    return () => {
      unsubscribe1();
      unsubscribe2();
    };
  }, []);

  return guilds;
}

export default function GuildList() {
  const guilds = useGuilds();
  const params = useParams();
  const history = useHistory();

  return (
    <nav className="GuildList" aria-label="Guild sidebar">
      <ul role="tree" aria-orientation="vertical">
        <GuildListItem
          className="GuildList-home-button"
          aria-label="Home"
          tooltip="Home"
          active={params.guildId === '@me'}
          onClick={() => history.push('/app/channels/@me/@me')}
        >
          <div className="GuildList-home-button-inner"></div>
        </GuildListItem>
        <GuildListSeparator />
        <div className="GuildList-guilds" aria-label="Guilds">
          {guilds.map((guild) => (
            <GuildListGuild key={guild.id} guild={guild} />
          ))}
        </div>
        <GuildListSeparator />
        <AddGuildButton />
      </ul>
    </nav>
  );
}

function AddGuildButton() {
  const [isOpen, setIsOpen] = useState(false);
  const [error, setError] = useState();
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');

  return (
    <GuildListItem
      className="GuildList-addguild-button"
      aria-label="Add a Guild"
      tooltip={<>Add&nbsp;a&nbsp;Guild</>}
      active={isOpen}
      onClick={() => {
        setIsOpen(true);
      }}
    >
      <div className="GuildList-addguild-button-inner">
        <RiAddFill />
      </div>
      <Modal
        onclose={() => {
          setTimeout(() => {
            setIsOpen(false);
            setName('');
            setDescription('');
          }, 0);
        }}
        shouldReturnFocusAfterClose={false}
        leftButton={
          <Button
            secondary
            onClick={() => {
              setTimeout(() => {
                setIsOpen(false);
                setName('');
                setDescription('');
              }, 0);
            }}
          >
            Cancel
          </Button>
        }
        isOpen={isOpen}
        rightButton={
          <Button
            brandcolor
            onClick={async () => {
              try {
                await axios.post('/guilds/', {
                  name,
                  description,
                  icon_url: null,
                  banner_url: null, // TODO: icon and banner
                  channels: [
                    { id: 1, type: 0, name: 'general' },
                    { id: 2, type: 0, name: 'off-topic' },
                    { id: 3, type: 0, name: 'memes' },
                  ], // TODO: guild templates of some sort
                });
              } catch (error) {
                setError(error);
              } finally {
                setTimeout(() => {
                  setIsOpen(false);
                  setName('');
                  setDescription('');
                }, 0);
              }
            }}
          >
            Create
          </Button>
        }
      >
        Give your new guild a profile with a name, a description, a banner and an icon. You can always change it later.
        <HtmlField
          type="text"
          name="guildname"
          className="Input TextField"
          label="GUILD NAME"
          onChange={(e) => setName(e.target.value)}
        />
        <HtmlField
          type="text"
          name="guilddescription"
          className="Input TextField"
          label="GUILD DESCRIPTION"
          onChange={(e) => setDescription(e.target.value)}
        />
      </Modal>
      {error && (
        <InfoModal
          onclose={() => {
            setError(null);
          }}
        >
          There was an error while creating the guild:
          <br />
          {error.toString()}
          <br />
          {error?.response?.data?.message &&
            typeof error.response.data.message === 'string' &&
            error.response.data.message}
        </InfoModal>
      )}
    </GuildListItem>
  );
}

function GuildListItem({ className = '', active, children, tooltip, childProps, ...props }) {
  className = className + ' GuildList-item-wrapper ' + (active ? 'GuildList-item-wrapper-active' : '');
  return (
    <Tooltip tooltip={tooltip} tooltipPlacement="right">
      <button
        className={className}
        {...props}
        style={{
          backgroundColor: 'transparent',
        }}
      >
        <div className="GuildList-item-pill-wrapper">
          <div
            className={`GuildList-item-pill`}
            style={
              active
                ? {
                    transform: 'translateX(-2px)',
                    height: '40px',
                  }
                : {}
            }
          ></div>
        </div>
        <div className="GuildList-item" {...childProps}>
          {children}
        </div>
      </button>
    </Tooltip>
  );
}
