import React from 'react';

import logoWordmark from '@assets/logos/logo-wordmark.svg';
import { Link, useLocation } from 'react-router-dom';
import { useLoginStateContext } from '@contexts/LoginStateContext';

import { LinkButton, AnchorLinkButton } from '@components/blocks/Button/Button';

import './NavBar.css';

function NavBar({ className = '' }) {
  const { loggedIn, logout } = useLoginStateContext();
  const showLogin = !loggedIn;

  const location = useLocation();
  return (
    <nav className={`NavBar ${className}`}>
      <Link to="/">
        <img className="nav-logo" src={logoWordmark} alt="Snowchat Logo" />
      </Link>
      <div className="NavBar-buttons">
        <LinkButton to="/" className="NavItem" secondary={location.pathname !== '/'}>
          Home
        </LinkButton>
        <LinkButton to="/download" className="NavItem" secondary={location.pathname !== '/download'}>
          Download
        </LinkButton>
        {showLogin && (
          <>
            <LinkButton to="/login" className="NavItem" secondary={location.pathname !== '/login'}>
              Login
            </LinkButton>
            <LinkButton to="/register" className="NavItem" secondary={location.pathname !== '/register'}>
              Register
            </LinkButton>
          </>
        )}
        {!showLogin && (
          <>
            <AnchorLinkButton onClick={logout} className="NavItem" secondary>
              Log Out
            </AnchorLinkButton>
            <LinkButton to="/app" className="NavItem" secondary>
              App
            </LinkButton>
          </>
        )}
      </div>
    </nav>
  );
}

export default NavBar;
