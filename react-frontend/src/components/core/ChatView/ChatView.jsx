import React, { useState, useEffect } from 'react';

import { useConn } from '@contexts/WebSocketContext';
import { useParams } from 'react-router';
import axios from 'axios';

export default function ChatView() {
  const conn = useConn();
  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState([]);

  const { guildId, channelId, ...params } = useParams();

  const handleSubmit = (e) => {
    e.preventDefault();
    setMessage('');
    if (!message.trim()) return;

    console.log(channelId, guildId);
    axios.post('/channels/' + channelId + '/messages', {
      content: message,
    });
  };

  const messageHandler = (data) => {
    setMessages(messages.concat([data]));
  };

  useEffect(() => {
    // on mount
    const unsubscribe = conn.addListener('MESSAGE_CREATE', messageHandler);
    return () => {
      // on unmount
      unsubscribe();
    };
  }, [messages]);

  return (
    <div className="ChatView">
      <ul className="ChatView-messages">
        {messages.map((message, index) => (
          <li className="ChatView-message" key={index}>
            {message?.author?.username}#{String(message?.author?.discriminator).padStart(4, '0')}: {message.content}
          </li>
        ))}
      </ul>
      <form onSubmit={handleSubmit}>
        <textarea
          name="msg"
          id="input-field-inner ChatView-msg"
          type="text"
          value={message}
          onKeyDown={(e) => e.key === 'Enter' && !e.shiftKey && handleSubmit(e)}
          onChange={(e) => setMessage(e.target.value)}
        ></textarea>
      </form>
    </div>
  );
}
