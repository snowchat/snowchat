import React, { useState, useEffect } from 'react';
import './App.css';

const Wrappers = React.lazy(() => import('@components/core/Wrappers'));
import cconsole from '@helpers/cconsole';

import logo from '@assets/logos/logo.svg';

import { LoginStateContext } from '@contexts/LoginStateContext';

import { Switch as RouteSwitch, Route, Redirect, useHistory, useLocation } from 'react-router-dom';

import { connect } from '@helpers/rawWS';
import { useToken } from '@hooks/useToken';

const HomePage = React.lazy(() => import('@components/pages/HomePage/HomePage'));
const Downloads = React.lazy(() => import('@components/pages/Downloads/Downloads'));
const Login = React.lazy(() => import('@components/pages/Login/Login'));
const Register = React.lazy(() => import('@components/pages/Register/Register'));
const BaseLayout = React.lazy(() => import('@components/core/BaseLayout/BaseLayout'));

import { WebSocketContext } from '@contexts/WebSocketContext';
import config from '@config';
import axios from 'axios';

function DevWarning() {
  return (
    <div className="DevWarning">
      <p>Snowchat is still in early development.</p>
    </div>
  );
}

function LoadingSpinner({ icon = true, text = 'Loading...', ...props }) {
  sync('loadingSpinnerAnimation');
  sync('loadingSpinnerAnimation2');
  return (
    <div className="loadingSpinnerWrapper" {...props}>
      {icon && (
        <div className="loadingSpinner">
          <img src={logo} alt="Snowchat Logo" width="100px" />
        </div>
      )}
      <h2>{text}</h2>
    </div>
  );
}

function LoadingSpinnerTest() {
  const [test, setTest] = useState(true);
  const [num, setNum] = useState(0);
  console.log('re-render', num, test, new Array(num).fill(null));
  return (
    <div
      className="LoadingSpinnerTest"
      onClick={() => {
        console.log('left click');
        setNum(num + 1);
      }}
      onContextMenu={(e) => {
        e.preventDefault();
        setTest(!test);
      }}
    >
      {test && <LoadingSpinner text="spinner 1" />}
      {new Array(num).fill(null).map((_, i) => {
        console.log('TESTTT', i, num);
        return (
          <LoadingSpinner
            text={'SPINNER ' + (i + 1)}
            style={{
              opacity: '0.5',
              transform: 'translateX(' + i * 10 + 'px)',
            }}
          />
        );
      })}
    </div>
  );
}

function PrivateRoute({ children, canAccess, redirect = '/login', ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        canAccess ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: redirect,
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}

function App() {
  //#region Authentication and WebSocket stuff
  const [connection, setConnection] = useState(null);
  var [token, setToken] = useToken(); // has to be var to work in getToken()

  const [loggedIn, setLoggedIn] = useState(!!localStorage.getItem('token'));
  const [authLoading, setAuthLoading] = useState(false);

  let history = useHistory();
  let location = useLocation();

  //#region functions

  async function login(email, password) {
    cconsole.logc('Authentication', 'Log In');
    setAuthLoading(true);
    setLoggedIn(false);

    let response;
    let error;

    try {
      response = await getToken(email, password);
    } catch (responseError) {
      error = responseError;
    }

    const successful = !!response;
    error = error || !response;

    if (error) {
      setToken(null);
    }

    setLoggedIn(successful);

    setAuthLoading(false);

    if (successful) {
      history.push('/app');
    }

    return error;
  }

  async function register(values) {
    cconsole.logc('Authentication', 'Create Account');
    setAuthLoading(true);
    setLoggedIn(false);

    try {
      await axios.post('/auth/register', {
        username: values.username,
        password: values.password,
        email: values.email,
      });
    } catch (error) {
      cconsole.error('Authentication', 'There was an error while creating the new account', error);

      setAuthLoading(false);
      setLoggedIn(false);
      return [error, false];
    }

    var loginError = await login(values.email, values.password);

    if (loginError) {
      cconsole.error('Authentication', 'There was an error while logging in', error);
    }

    setAuthLoading(false);
    setLoggedIn(true);

    return [loginError, true];
  }

  function logout() {
    cconsole.logc('Authentication', 'Log Out');
    if (loggedIn) history.push('/');
    setAuthLoading(false);
    setLoggedIn(false);
    setToken(null);
    // if ws connection (open or closed): try close it
    if (connection) {
      connection.close();
    }
  }

  async function getToken(email, password) {
    cconsole.infoc('Authentication', 'getToken()');
    var tempToken = token;

    //#region Get token if credentials exist and token does not exist
    if (email && password && !tempToken) {
      cconsole.debugc('Authentication', 'Credentials exist but token does not');
      const res = await axios.post('/auth/login', {
        email,
        password,
      });
      cconsole.debugc('Authentication', 'Got token');
      tempToken = res.data.token;
    }
    //#endregion

    //#region Check if token valid
    if (!tempToken) {
      cconsole.infoc(
        'Authentication',
        'No token, need to re-login to get new token',
        localStorage.getItem('token'),
        localStorage,
      );
      logout();
      return false;
    }
    //#endregion

    setToken(tempToken);

    //#region Connect WebSocket if not already connected
    cconsole.infoc('WebSocket', 'Try to connect', !connection || connection?.isClosed, !!tempToken);
    if ((!connection || connection?.isClosed) && !!tempToken) {
      connect(tempToken, logout)
        .then((connection) => setConnection(connection))
        .catch((...args) => cconsole.errorc('WebSocket', ...args));
    }
    //#endregion

    return tempToken;
  }

  //#endregion

  useEffect(() => {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    return () => {
      delete axios.defaults.headers.common['Authorization'];
    };
  }, [token]);

  useEffect(() => {
    axios.defaults.baseURL = config.api_url;

    // try get tokens
    getToken().catch((err) => {
      console.error(err);
      console.error(err?.response?.data?.error);
      logout();
    });

    return () => {
      delete axios.defaults.baseURL;
      // if ws connection (open or closed): try close it
      if (connection) {
        connection.close();
      }
    };
  }, []);

  //#endregion

  return (
    <div className="App">
      {location.pathname.startsWith('/app/channels') && authLoading && <LoadingSpinner text="Authenticating..." />}
      {location.pathname.startsWith('/app/channels') && !connection && (
        <LoadingSpinner text="Connecting WebSocket..." />
      )}
      {location.pathname.startsWith('/app/channels') && connection && connection.isClosed && (
        <LoadingSpinner text="WebSocket Connection Closed" />
      )}
      <React.Suspense fallback={<LoadingSpinner icon={false} text="Loading Components..." />}>
        <LoginStateContext.Provider
          value={{
            login: login,
            register: register,
            logout: logout,
            loggedIn: loggedIn,
            token: token,
          }}
        >
          <WebSocketContext.Provider value={connection}>
            <Wrappers>
              <RouteSwitch>
                <Route exact path="/download">
                  <React.Suspense
                    fallback={
                      <div className="pathLoading">
                        <h2>Loading...</h2>
                      </div>
                    }
                  >
                    <Downloads />
                  </React.Suspense>
                </Route>
                <Route exact path="/login">
                  <React.Suspense
                    fallback={
                      <div className="pathLoading">
                        <h2>Loading...</h2>
                      </div>
                    }
                  >
                    <Login />
                  </React.Suspense>
                </Route>
                <Route exact path="/register">
                  <React.Suspense
                    fallback={
                      <div className="pathLoading">
                        <h2>Loading...</h2>
                      </div>
                    }
                  >
                    <Register />
                  </React.Suspense>
                </Route>

                <Route
                  exact
                  path="/app"
                  render={({ location }) => (
                    <Redirect
                      to={{
                        pathname: loggedIn ? '/app/channels/@me/@me' : '/login',
                        state: { from: location },
                      }}
                    />
                  )}
                />
                {/* TODO: /app/guild-discovery for guild discovery */}
                {/* TODO: /app/stage-discovery for stage discovery */}
                {/* TODO: /app/store for donations */}
                <PrivateRoute exact path="/app/channels/:guildId/:channelId" canAccess={loggedIn}>
                  {!authLoading && connection && !connection.isClosed && <BaseLayout />}
                </PrivateRoute>
                <Route exact path="/">
                  <React.Suspense
                    fallback={
                      <div className="pathLoading">
                        <h2>Loading...</h2>
                      </div>
                    }
                  >
                    <HomePage />
                  </React.Suspense>
                </Route>
                <Route
                  /* 404 handler */
                  path="/"
                  render={({ location }) => (
                    <Redirect
                      to={{
                        pathname: '/',
                        state: { from: location },
                      }}
                    />
                  )}
                />
              </RouteSwitch>
            </Wrappers>
          </WebSocketContext.Provider>
        </LoginStateContext.Provider>
      </React.Suspense>
      <DevWarning />
    </div>
  );
}

export default App;
