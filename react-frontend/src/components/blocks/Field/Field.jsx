import React from 'react';
import { Field as FormikField, ErrorMessage } from 'formik';

import './Field.css';

export default function Field({ labelIcon, label, ...props }) {
  return (
    <div className="Field input-field">
      <label htmlFor={'field-id-' + props.name} className="input-field-label">
        {labelIcon}
        {label}
      </label>
      <FormikField {...props} id={'field-id-' + props.name} className="input-field-inner" />
      <ErrorMessage name={props.name} component="div" className="input-field-error" />
    </div>
  );
}

export function HtmlField({ labelIcon, label, ...props }) {
  return (
    <div className="Field input-field">
      <label htmlFor={'field-id-' + props.name} className="input-field-label">
        {labelIcon}
        {label}
      </label>
      <input {...props} id={'field-id-' + props.name} className="input-field-inner" />
    </div>
  );
}
