import React from 'react';
import './Pfp.css';

import { FaCircle, FaMoon, FaMinusCircle, FaDotCircle } from 'react-icons/fa';

import config from '@config';

function statusToString(status) {
  return status === 'online'
    ? 'Online'
    : status === 'idle'
    ? 'Idle'
    : status === 'dnd'
    ? 'Do Not Disturb'
    : status === 'offline'
    ? 'Offline'
    : 'Online';
}

function IconWithColor({ icon, color, style, ...props }) {
  return (
    <div
      style={{
        color,
        ...style,
      }}
      {...props}
    >
      {icon}
    </div>
  );
}

function StatusIcon({ status, ...props }) {
  return status === 'online' ? (
    <IconWithColor color="rgb(59, 165, 93)" icon={<FaCircle />} {...props} /> // online
  ) : status === 'idle' ? (
    <IconWithColor color="rgb(250, 168, 26)" icon={config.status_only_circle ? <FaCircle /> : <FaMoon />} {...props} /> // idle
  ) : status === 'dnd' ? (
    <IconWithColor
      color="rgb(237, 66, 69)"
      icon={config.status_only_circle ? <FaCircle /> : <FaMinusCircle />}
      {...props}
    /> // dnd
  ) : status === 'offline' ? (
    <IconWithColor
      color="rgb(116, 127, 141)"
      icon={config.status_only_circle ? <FaCircle /> : <FaDotCircle />}
      {...props}
    /> // offline
  ) : (
    <IconWithColor color="rgb(59, 165, 93)" icon={config.status_only_circle ? <FaCircle /> : <FaCircle />} {...props} /> // online
  );
}

export default function Pfp({ username, className, status, src, ...props }) {
  className = className ? className + ' Pfp' : 'Pfp';
  return (
    <div role="img" aria-label={`${username}, ${statusToString(status)}`} className="Pfp-wrapper" {...props}>
      {status ? (
        <svg width="32" height="32" viewBox="0 0 32 32" aria-hidden="true">
          <mask id="svg-mask-avatar-status-round-32" maskContentUnits="objectBoundingBox" viewBox="0 0 1 1">
            <circle fill="white" cx="0.5" cy="0.5" r="0.5"></circle>
            <circle fill="black" cx="0.84375" cy="0.84375" r="0.25"></circle>
          </mask>
          <foreignObject x="0" y="0" width="32" height="32" mask="url(#svg-mask-avatar-status-round-32)">
            <div
              className={className}
              style={{
                backgroundImage: src,
              }}
            ></div>
          </foreignObject>
          {/* thing here
        <rect
          width="10"
          height="10"
          x="22"
          y="22"
          fill="hsl(359, calc(var(--saturation-factor, 1) * 82.6%), 59.4%)"
          mask="url(#svg-mask-status-dnd)"
          class="pointerEvents-2zdfdO"></rect>
        */}
          <foreignObject x="22" y="22" width="10" height="10">
            <StatusIcon
              status={status}
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                width: '100%',
                height: '100%',
              }}
            />
          </foreignObject>
        </svg>
      ) : (
        <div
          className={className}
          style={{
            backgroundImage: src,
          }}
        ></div>
      )}
    </div>
  );
}
