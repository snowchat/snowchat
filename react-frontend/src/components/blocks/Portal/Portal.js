import React from 'react';
import ReactDOM from 'react-dom';

class Portal extends React.Component {
  constructor(props) {
    super(props);
    this.portalRoot = props.portalRoot;
    this.el = document.createElement('div');
  }

  componentDidMount = () => {
    this.portalRoot.appendChild(this.el);
  };

  componentWillUnmount = () => {
    this.portalRoot.removeChild(this.el);
  };

  render() {
    const { children } = this.props;
    return ReactDOM.createPortal(children, this.el);
  }
}

export default Portal;
