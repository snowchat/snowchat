import React, { useState } from 'react';
import './Modal.css';
import Button from '@components/blocks/Button/Button';
import ReactModal from 'react-modal';

ReactModal.setAppElement('#root');

export default function Modal({
  onclose,
  wrapperComponent,
  isOpen,
  children,
  leftButton,
  centerButton,
  rightButton,
  ...props
}) {
  function closeModal() {
    onclose && onclose();
  }
  var WrapperComponent = wrapperComponent || React.Fragment;
  return (
    <ReactModal
      isOpen={isOpen}
      onAfterOpen={() => null}
      onRequestClose={closeModal}
      style={{
        overlay: {
          background: '',
        },
        content: {
          background: 'transparent',
          border: 'none',
        },
      }}
      {...props}
      className="modal-body"
      contentLabel="Example Modal"
      closeTimeoutMS={300 /* This is to fix the closing animation */}
    >
      <WrapperComponent>
        <div className="modal-content">{children}</div>
        <div className="modal-buttons">
          <div className="left">{leftButton}</div>
          <div className="center">{centerButton}</div>
          <div className="right">{rightButton}</div>
        </div>
      </WrapperComponent>
    </ReactModal>
  );
}

/*export default function Modal(props) {
  function closeModal() {
    props.onclose && props.onclose();
  }
  const rootEl = useRef(null);
  var WrapperComponent = props.wrapperComponent || React.Fragment;
  useEffect(() => {
    let focusableEls = rootEl.current.querySelectorAll(
      'a[href], area[href], input:not([disabled]), select:not([disabled]), ' +
        'textarea:not([disabled]), button:not([disabled]), [tabindex="0"]',
    );
    focusableEls[0].focus();
    let focusedElBeforeOpen = document.activeElement;
    return () => {
      focusedElBeforeOpen.focus();
    };
  }, []);
  return (
    <Portal portalRoot={modalRoot}>
      <div className="Modal">
        <div className="modal-backdrop" onClick={closeModal}></div>
        <div className="modal-body" role="dialog" aria-modal="true" ref={rootEl}>
          <WrapperComponent>
            <div className="modal-content">{props.children}</div>
            <div className="modal-buttons">
              <div className="left">{props.leftButton}</div>
              <div className="center">{props.centerButton}</div>
              <div className="right">{props.rightButton}</div>
            </div>
          </WrapperComponent>
        </div>
      </div>
    </Portal>
  );
}*/

export function YesNoModal(props) {
  const [isOpen, setIsOpen] = useState(true);
  function closeModal() {
    setIsOpen(false);
    props.onclose && props.onclose();
  }
  const no = () => {
    props.onno && props.onno();
    closeModal();
  };
  const yes = () => {
    props.onyes && props.onyes();
    closeModal();
  };
  return isOpen ? (
    <Modal
      onclose={closeModal}
      leftButton={
        <>
          <Button onClick={yes}>Yes</Button>
          <Button secondary onClick={no}>
            No
          </Button>
        </>
      }
    >
      {props.children}
    </Modal>
  ) : null;
}
export function InfoModal(props) {
  const [isOpen, setIsOpen] = useState(true);
  function closeModal() {
    setIsOpen(false);
    props.onclose && props.onclose();
  }
  return (
    <Modal
      isOpen={isOpen}
      onclose={closeModal}
      leftButton={
        <Button secondary onClick={closeModal}>
          Cancel
        </Button>
      }
      rightButton={
        <Button brandcolor onClick={closeModal}>
          Okay
        </Button>
      }
    >
      {props.children}
    </Modal>
  );
}
