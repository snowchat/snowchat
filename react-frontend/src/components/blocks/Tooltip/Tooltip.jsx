import React from 'react';
import './Tooltip.css';

export default function Tooltip({ children, tooltip, tooltipPlacement = 'top', ...props }) {
  return (
    <div className="Tooltip" {...props}>
      <div className={`Tooltip-child Tooltip-${tooltipPlacement}`} aria-hidden="true">
        {tooltip}
      </div>
      {children}
    </div>
  );
}
