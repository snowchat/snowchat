import React from 'react';
import './Separator.css';

export default function Separator({ thickness = '1px', ...props }) {
  return (
    <div
      className="Separator"
      role="separator"
      style={{
        borderBottomWidth: thickness,
      }}
      {...props}
    ></div>
  );
}
