import React from 'react';

import './Button.css';

export default function Button({ brandcolor, secondary, large, wide, className, type, ...props }) {
  var classes = 'Button btn font-medium';

  if (brandcolor) classes += ' btn-brandcolor';
  if (secondary) classes += ' btn-secondary';
  if (large) classes += ' btn-large';
  if (wide) classes += ' btn-wide';
  if (className) classes += ' ' + className;

  return (
    <button className={classes} type={type || 'button'} {...props}>
      {props.children}
    </button>
  );
}

import { Link as RouteLink } from 'react-router-dom';

export function LinkButton({ brandcolor, secondary, large, wide, className, ...props }) {
  var classes = 'LinkButton Button btn font-medium';

  if (brandcolor) classes += ' btn-brandcolor';
  if (secondary) classes += ' btn-secondary';
  if (large) classes += ' btn-large';
  if (wide) classes += ' btn-wide';
  if (className) classes += ' ' + className;

  return (
    <RouteLink className={classes} {...props}>
      {props.children}
    </RouteLink>
  );
}

export function AnchorLinkButton({ brandcolor, secondary, large, wide, className, ...props }) {
  var classes = 'LinkButton Button btn font-medium';

  if (brandcolor) classes += ' btn-brandcolor';
  if (secondary) classes += ' btn-secondary';
  if (large) classes += ' btn-large';
  if (wide) classes += ' btn-wide';
  if (className) classes += ' ' + className;

  return (
    <a className={classes} {...props}>
      {props.children}
    </a>
  );
}
