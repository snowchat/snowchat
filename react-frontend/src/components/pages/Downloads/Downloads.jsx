import React from 'react';

import utils from '@helpers/util';

import { AnchorLinkButton } from '@components/blocks/Button/Button';

import './Downloads.css';

import { RiDownload2Line, RiGitlabLine, RiDiscordFill } from 'react-icons/ri';

const NavBar = React.lazy(() => import('@components/core/NavBar/NavBar'));

function Downloads() {
  const OS = utils.getOperatingSystem();

  return (
    <>
      <div className="Downloads-wrapper">
        <NavBar />
        <div className="Downloads">
          <h1>Download</h1>
          <div className="Downloads-columns">
            <div className={`col ${OS === 'Windows' ? 'col-active' : ''}`}>
              <h2>
                <a id="win">Windows</a>
              </h2>
              <AnchorLinkButton secondary href="/api/download?platform=win&format=.exe">
                <RiDownload2Line /> Installer (.exe){' '}
              </AnchorLinkButton>
              <AnchorLinkButton secondary href="/api/download?platform=win&format=.zip">
                <RiDownload2Line /> Portable (.zip){' '}
              </AnchorLinkButton>
            </div>
            <div className={`col ${OS === 'Linux' ? 'col-active' : ''}`}>
              <h2>
                <a id="linux">Linux</a>
              </h2>
              <AnchorLinkButton secondary href="/api/download?platform=linux&format=.deb">
                <RiDownload2Line /> Installer (.deb){' '}
              </AnchorLinkButton>
              <AnchorLinkButton secondary href="/api/download?platform=linux&format=.tar.gz">
                <RiDownload2Line /> Portable (.tar.gz){' '}
              </AnchorLinkButton>
            </div>
            <div className={`col ${OS === 'MacOS' ? 'col-active' : ''}`}>
              <h2>
                <a id="osx">MacOS</a>
              </h2>
              <AnchorLinkButton secondary href="/api/download?platform=osx">
                <RiDownload2Line /> Installer (.dmg){' '}
              </AnchorLinkButton>
            </div>
          </div>
        </div>
        <footer className="footer">
          <a href="https://gitlab.com/snowchat/snowchat/-/issues">Report a bug</a>
          <div className="footer-logos">
            <a href="https://gitlab.com/snowchat">
              <RiGitlabLine className="gitlab-logo" />
            </a>
            <a href="https://discord.gg/gmMNzdyvM7">
              <RiDiscordFill className="discord-logo" />
            </a>
          </div>
        </footer>
      </div>
    </>
  );
}

export default Downloads;
