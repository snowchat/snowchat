import React from 'react';

import './HomePage.css';

import { LinkButton } from '@components/blocks/Button/Button';

import { RiDownload2Line, RiGlobalLine, RiGitlabLine, RiDiscordFill } from 'react-icons/ri';

const NavBar = React.lazy(() => import('@components/core/NavBar/NavBar'));

function HomePage() {
  return (
    <div className="HomePage">
      <NavBar />
      <header className="homepage-hero">
        <h1>Snowchat</h1>
        <p>The Open-Source Chat Platform</p>
        <div className="homepage-hero-cta-group">
          <LinkButton brandcolor large to="/download">
            <RiDownload2Line /> Download
          </LinkButton>
          <LinkButton brandcolor large to="/app">
            <RiGlobalLine /> Open in your browser
          </LinkButton>
        </div>
      </header>
      <footer className="footer">
        <a href="https://gitlab.com/snowchat/snowchat/-/issues">Report a bug</a>
        <div className="footer-logos">
          <a href="https://gitlab.com/snowchat">
            <RiGitlabLine className="gitlab-logo" />
          </a>
          <a href="https://discord.gg/gmMNzdyvM7">
            <RiDiscordFill className="discord-logo" />
          </a>
        </div>
      </footer>
    </div>
  );
}

export default HomePage;
