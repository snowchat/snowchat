import React, { useState } from 'react';
import { Formik, Form } from 'formik';
const Button = React.lazy(() => import('@components/blocks/Button/Button'));
const Field = React.lazy(() => import('@components/blocks/Field/Field'));

import { RiLoginCircleLine, RiLock2Line, RiUser3Line } from 'react-icons/ri';

import { Link as RouteLink } from 'react-router-dom';

const LoginPopup = React.lazy(() => import('@components/core/LoginPopup/LoginPopup'));
import { useLoginStateContext } from '@contexts/LoginStateContext';

function Login() {
  const [unSuccessful, setUnSuccessful] = useState('');
  const { login } = useLoginStateContext();

  return (
    <LoginPopup>
      <div className="login-login">
        <Formik
          initialValues={{ email: '', password: '' }}
          validate={(values) => {
            const errors = {};

            if (!values.email) {
              errors.email = 'E-Mail is required';
            } else if (!values.email.match(/^.{5,100}$/)) {
              errors.email = 'E-Mail has to be between 5 and 100 characters long';
            }

            if (!values.password) {
              errors.password = 'Password is required';
            } else if (!values.password.match(/^.{8,72}$/)) {
              errors.password = 'Password has to be between 8 and 72 characters long';
            }

            return errors;
          }}
          onSubmit={(values, { setSubmitting, setErrors }) => {
            setTimeout(async () => {
              // alert(JSON.stringify(values, null, 2));

              console.info('signing in');

              const error = await login(values.email, values.password);

              if (error) {
                if (error?.response?.data?.errors && typeof error.response.data.errors === 'object') {
                  function objectMap(object, mapFn) {
                    return Object.keys(object).reduce((acc, k) => {
                      acc[k] = mapFn(k, object[k]);
                      return acc;
                    }, {});
                  }
                  setErrors(
                    objectMap(error.response.data.errors, (fieldName, { _errors }) =>
                      _errors.map((e) => e.message || e.code).join(', '),
                    ),
                  );
                } else if (error?.response?.data?.error) {
                  setUnSuccessful(error.response.data.message);
                } else {
                  console.log(error);
                  setUnSuccessful('Something went wrong while logging you in...');
                }
                setSubmitting(false);
                return;
              }

              setUnSuccessful('');

              setSubmitting(false);
            }, 400);
          }}
        >
          {({ isSubmitting }) => (
            <Form>
              <h2
                style={{
                  margin: '0rem 0 1.5rem 0',
                }}
              >
                Log in
              </h2>
              <Field
                type="text"
                name="email"
                className="Input TextField"
                labelIcon={<RiUser3Line />}
                label="E-Mail"
                autoComplete="email"
              />

              <Field
                type="password"
                name="password"
                className="Input TextField"
                labelIcon={<RiLock2Line />}
                label="Password"
                autoComplete="current-password"
              />

              <Button type="submit" disabled={isSubmitting} brandcolor large wide style={{ marginTop: '1.8rem' }}>
                <RiLoginCircleLine /> Login
              </Button>
              <RouteLink to="/register" className="login-form-link dont-have-account">
                Don't have an account?
              </RouteLink>
            </Form>
          )}
        </Formik>
        {unSuccessful && <div className="login-unsuccessful">{unSuccessful}</div>}
      </div>
    </LoginPopup>
  );
}

export default Login;
