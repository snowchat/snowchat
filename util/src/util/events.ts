import { Channel } from 'amqplib';
import { RabbitMQ } from './rabbitmq';
import { Event, EventType } from '../interfaces/event';

export async function emitEvent(payload: Omit<Event, 'created_at'>) {
  console.log('[Util] Sending event', payload);
  // Get ID/channel
  const id = (payload.channel_id || payload.user_id || payload.guild_id) as string;
  if (!id) return console.error("[Events] Event doesn't contain any id", payload);

  // Encode JSON
  const data = typeof payload.data === 'object' ? JSON.stringify(payload.data) : payload.data;
  await RabbitMQ.channel?.assertExchange(id, 'fanout', { durable: false });

  // assertQueue isn't needed, because a queue will automatically created if it doesn't exist
  const successful = RabbitMQ.channel?.publish(id, '', Buffer.from(`${data}`), { type: payload.event });
  if (!successful) throw new Error('failed to send event');
}

export async function initEvent() {
  await RabbitMQ.init(); // does nothing if rabbitmq is not setup
}

export interface EventOpts extends Event {
  ack?: Function;
  channel?: Channel;
  cancel: Function;
}

export interface ListenEventOpts {
  channel?: Channel;
  ack?: boolean;
}

export async function listenEvent(event: string, callback: (event: EventOpts) => any, opts?: ListenEventOpts) {
  // @ts-ignore
  return rabbitmqListen(opts?.channel || RabbitMQ.channel, event, callback, { ack: opts?.ack });
}

async function rabbitmqListen(
  channel: Channel,
  id: string,
  callback: (event: EventOpts) => any,
  opts?: { ack?: boolean },
) {
  await channel.assertExchange(id, 'fanout', { durable: false });
  const q = await channel.assertQueue('', { exclusive: true, autoDelete: true });

  const cancel = () => {
    channel.cancel(q.queue);
    channel.unbindQueue(q.queue, id, '');
  };

  channel.bindQueue(q.queue, id, '');
  channel.consume(
    q.queue,
    (opts) => {
      if (!opts) return;

      const data = JSON.parse(opts.content.toString());
      const event = opts.properties.type as EventType;

      callback({
        event,
        data,
        ack() {
          channel.ack(opts);
        },
        channel,
        cancel,
      });
      // rabbitCh.ack(opts);
    },
    {
      noAck: !opts?.ack,
    },
  );

  return cancel;
}
