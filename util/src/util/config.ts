import { Snowflake } from './Snowflake';
import fs from 'fs';
import crypto from 'crypto';
import path from 'path';

export const defaultConfig = {
  general: {
    instance_id: Snowflake.generate(),
  },
  permissions: {
    user: {
      createGuilds: true, // TODO: user can create guild instance-wide permission
    },
  },
  limits: {
    // TODO: user limits
    user: {
      maxJoinedGuilds: 100,
      minUsernameLength: 3,
      maxUsernameLength: 32,
      maxFriends: 1000,
    },
    guild: {
      maxRoles: 100,
      maxMembers: 50000,
      maxChannels: 100,
      hideOfflineMembers: 1000,
      maxChannelsCategory: 20,
    },
    message: {
      maxLength: 2000,
      maxDonorLength: 4000,
      maxReactions: 40,
    },
    channel: {
      maxPins: 75,
      maxDescriptionLength: 1024,
    },
    rate: {
      // TODO: implement rate limits
      // count = requests
      // window = seconds
      // bot = count but for bots
      // GET = GET, OPTIONS, HEAD overrides
      // MODIFY = POST, DELETE, PATCH, PUT overrides
      ip: {
        count: 500,
        window: 5,
      },
      global: {
        count: 20,
        window: 5,
        bot: 250,
      },
      error: {
        count: 10,
        window: 5,
      },
      routes: {
        guild: {
          count: 5,
          window: 5,
        },
        webhook: {
          count: 5,
          window: 20,
        },
        channel: {
          count: 5,
          window: 20,
        },
        auth: {
          login: {
            count: 5,
            window: 60,
          },
          register: {
            count: 2,
            window: 60 * 60 * 12,
          },
        },
      },
    },
  },
  security: {
    autoUpdate: true,
    // This is for connecting with other API services like the CDN
    requestSignature: crypto.randomBytes(32).toString('base64'),
    jwtSecret: crypto.randomBytes(256).toString('base64'),
    // forwadedFor: "X-Forwarded-For" // nginx/reverse proxy
    // forwadedFor: "CF-Connecting-IP" // cloudflare
    forwadedFor: null,
    // TODO: recaptcha/hcaptcha
  },
  register: {
    email: {
      // TODO: email blacklist
      blacklist: true,
    },
    dateOfBirth: {
      // TODO: minimum age
      necessary: true,
      minimum: 13,
    },
    allowNewRegistration: true,
    password: {
      // TODO: password limits
      minLength: 8,
      minNumbers: 0,
      minUpperCase: 0,
      minSymbols: 0,
      maxLength: 72,
    },
    requireInvite: false, // TODO: require invite to register
  },
  rabbitmq: {
    host: process.env.NODE_ENV == 'production' ? 'amqp://rabbitmq' : 'amqp://localhost',
  },
};

const configFile = process.env.CONFIG_FILE || path.join(__dirname, '..', '..', 'config.json');

export const config = {
  config: defaultConfig,
  configInitialized: false,
  set() {
    'set';
  },
  init() {
    if (this.configInitialized) return;
    this.configInitialized = true;
    this._loadConfig(false);
    this._saveConfig();
  },
  get() {
    return this.config;
  },
  default: defaultConfig,
  _loadConfig(showWarning = true) {
    try {
      const loadedConfigText = fs.readFileSync(configFile, {
        encoding: 'utf8',
      });
      const loadedConfig = JSON.parse(loadedConfigText);
      this.config = {
        ...defaultConfig,
        ...loadedConfig,
      };
    } catch (error) {
      if (showWarning) {
        console.warn('Loading config file failed, using default config!');
        this.config = defaultConfig;
      }
    }
    return this.config;
  },
  _saveConfig() {
    try {
      fs.writeFileSync(configFile, JSON.stringify(this.config, null, '  '));
    } catch (error) {
      console.error('Saving config file failed!', error);
      this.config = defaultConfig;
    }
    return this.config;
  },
};
