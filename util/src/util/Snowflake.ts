import cluster from 'cluster';
import { EPOCH } from './constants';

export class Snowflake {
  static INCREMENT = BigInt(0); // maximum 4095
  static processId = BigInt(process.pid % 31); // maximum 31
  static workerId = BigInt((cluster.worker?.id || 0) % 31); // maximum 31

  constructor() {}

  static generate() {
    var time = BigInt(Date.now() - EPOCH) << BigInt(22);
    var worker = Snowflake.workerId << BigInt(17);
    var process = Snowflake.processId << BigInt(12);
    var increment = Snowflake.INCREMENT++;
    return (time | worker | process | increment).toString();
  }

  static deconstruct(snowflake: string): object {
    const binary = BigInt(snowflake).toString(2).padStart(64, '0');
    const res = {
      timestamp: parseInt(binary.substring(0, 42), 2) + EPOCH,
      rawTimestamp: parseInt(binary.substring(0, 42), 2) + EPOCH,
      workerID: parseInt(binary.substring(42, 47), 2),
      processID: parseInt(binary.substring(47, 52), 2),
      increment: parseInt(binary.substring(52, 64), 2),
      binary: binary,
      get date() {
        return new Date(this.timestamp);
      },
    };
    return res;
  }
}
