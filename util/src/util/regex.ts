export const SPECIAL_CHAR = /[@#`:\r\n\t\f\v\p{C}]/gu;
export const CHANNEL_MENTION = /<#(\d+)>/g;
export const USER_MENTION = /<@!?(\d+)>/g;
export const ROLE_MENTION = /<@&(\d+)>/g;
export const EVERYONE_MENTION = /@everyone/g;
export const HERE_MENTION = /@here/g;
export const EMAIL_REGEX =
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
