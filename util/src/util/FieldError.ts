import { objectMap } from './functions';

export class FieldError extends Error {
  constructor(public message: string, public errors?: any, public statusCode?: number) {
    super(message);
  }
}

export function FieldErrors(
  fields: Record<string, { code?: string; message: string }>,
  statusCode?: number,
  message: string = 'Invalid Form Body',
) {
  return new FieldError(
    message,
    objectMap(fields, ({ message, code }) => ({
      _errors: [
        {
          message,
          code: code || 'BASE_TYPE_INVALID',
        },
      ],
    })),
    statusCode,
  );
}
