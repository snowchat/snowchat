import amqp, { Connection, Channel } from 'amqplib';
import { config } from './config';

export const RabbitMQ: { connection: Connection | null; channel: Channel | null; init: () => Promise<void> } = {
  connection: null,
  channel: null,
  init: async function () {
    const host = config.get().rabbitmq.host;
    if (!host) return;
    console.log(`[RabbitMQ] connect: ${host}`);
    this.connection = await amqp.connect(host, {
      timeout: 1000 * 60,
    });
    console.log(`[RabbitMQ] connected`);
    this.channel = await this.connection.createChannel();
    console.log(`[RabbitMQ] channel created`);
  },
};

RabbitMQ.init();
