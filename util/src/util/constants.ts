import { VerifyOptions } from 'jsonwebtoken';

// 2021-01-01T00:00:00.000Z
export const EPOCH = 1_609_459_200_000;

export const JWTOptions: VerifyOptions = {
  algorithms: ['HS256'],
};

export const MessageTypes = {
  DEFAULT: 0,
  CALL: 1,
  GUILD_MEMBER_JOIN: 2,
  BOOST: 3,
  BOOST_TIER_1: 4,
  BOOST_TIER_2: 5,
  BOOST_TIER_3: 6,
  CHANNEL_FOLLOW_ADD: 7,
  REPLY: 8,
  EVENT_REMINDER: 9,
  VOTE: 10,
};

export const AllowedImageFormats = ['webp', 'png', 'jpg', 'jpeg', 'gif', 'apng'];

export const AllowedImageSizes = Array.from({ length: 9 }, (e, i) => 2 ** (i + 4));

/**
 * The current status of the client
 * @typedef {number} Status
 */
export const Status = {
  READY: 0,
  IDENTIFYING: 1,
  WAITING_FOR_READY: 2,
};
