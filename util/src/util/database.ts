import 'reflect-metadata';
import { Connection, createConnection } from 'typeorm';
import * as entities from '../entities';

var connectionPromise: Promise<Connection>;

export function initDatabase() {
  if (connectionPromise) return connectionPromise; // prevent initalizing multiple times

  console.log('[Database] Initializing Database');

  // @ts-ignore
  connectionPromise = createConnection({
    type: 'postgres',
    url: process.env.DATABASE_URL || 'postgres://snowy:example@localhost/snowchat',
    entities: Object.values(entities).filter((x) => x.constructor.name !== 'Object'),
    synchronize: true,
    logging: process.env.DATABASE_DEBUG === 'true',
  })
    .then((conn) => {
      console.log('[Database] Fully Initialized Database');
      return conn;
    })
    .catch((err) => {
      console.error('[Database] Error!', err);
    });

  return connectionPromise;
}

initDatabase();
