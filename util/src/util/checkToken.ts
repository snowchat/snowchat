import { JWTOptions } from './constants';
import jwt from 'jsonwebtoken';
import { User } from '../entities';
import { config } from './config';

export function checkToken(token: string): Promise<{
  decoded: any;
  user: User;
}> {
  token = token.replace(/^Bearer /g, '');
  return new Promise((resolve, reject) => {
    jwt.verify(token, config.get().security.jwtSecret, JWTOptions, async (err, decoded: any) => {
      if (err || !decoded) return reject('Invalid Token');

      const user = await User.findOne({ id: decoded.id }, { select: ['disabled', 'deleted', 'valid_tokens_since'] });
      if (!user) return reject('Invalid Token');
      if (decoded.iat * 1000 < user.valid_tokens_since.setSeconds(0, 0)) return reject('Invalid Token');
      if (user.disabled) return reject('User disabled');
      if (user.deleted) return reject('User not found');

      return resolve({ decoded, user });
    });
  });
}
