import { SPECIAL_CHAR } from './regex';

export function toBigInt(string: string): bigint {
  return BigInt(string);
}

export function trimSpecial(str?: string): string {
  // @ts-ignore
  if (!str) return;
  return str.replace(SPECIAL_CHAR, '').trim();
}

export function objectMap(object: object, fn: (v: any, k?: any, i?: number) => any) {
  const mapFn = ([k, v]: [any, any], i: number) => [k, fn(v, k, i)];
  return Object.fromEntries(Object.entries(object).map(mapFn));
}
