import { BaseEntity, PrimaryColumn, FindConditions } from 'typeorm';
import 'reflect-metadata';

// TODO: validate on paths because ajv and typescript-json-schema don't support bigints but the API sends them in strings

export class CustomBaseEntityWithoutId extends BaseEntity {
  constructor(private props: any = {}) {
    super();
    this.assign(props);
  }
  get construct(): any {
    return this.constructor;
  }

  assign(props?: any) {
    if (!props || typeof props !== 'object') return;

    /*for (const key in props) {
      // @ts-ignore
      this[key] = props[key];
    }*/
    Object.assign(this, props);

    /*for (const key in props) {
      Object.defineProperty(this, key, { value: props[key] });
    }*/
  }

  get metadata() {
    return this.construct.getRepository().metadata;
  }

  toJSON(): any {
    // This will also work with JSON.stringify
    // See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify#description
    return Object.fromEntries(
      // @ts-ignore
      this.metadata.columns.map((columnMetadata) => [
        columnMetadata.propertyName,
        // @ts-ignore
        this[columnMetadata.propertyName],
      ]),
    );
  }

  static increment<T extends CustomBaseEntity>(
    conditions: FindConditions<T>,
    propertyPath: string,
    value: number | string,
  ) {
    const repository = this.getRepository();
    return repository.increment(conditions, propertyPath, value);
  }

  static decrement<T extends CustomBaseEntity>(
    conditions: FindConditions<T>,
    propertyPath: string,
    value: number | string,
  ) {
    const repository = this.getRepository();
    return repository.decrement(conditions, propertyPath, value);
  }
}

export class CustomBaseEntity extends CustomBaseEntityWithoutId {
  @PrimaryColumn()
  id: string;
}
