import { HTTPError } from 'lambert-server';
import { Column, Entity, ManyToOne, JoinColumn, RelationId, OneToMany } from 'typeorm';
import { ChannelCreateEvent } from '../interfaces/guildEvents';
import { emitEvent } from '../util/events';
import { Snowflake } from '../util/Snowflake';
import { CustomBaseEntity } from './CustomBaseEntity';
import { Guild } from './Guild';
import { Message } from './Message';

export const ChannelTypes = {
  TEXT: 0,
  VOICE: 1,
  DM: 2,
  STAGE: 3,
  KANBAN: 4,
  CALENDAR: 5,
  CATEGORY: 6,
};
export enum ChannelType {
  TEXT = 0,
  VOICE = 1,
  DM = 2,
  STAGE = 3,
  KANBAN = 4,
  CALENDAR = 5,
  CATEGORY = 6,
}

// TODO: voice channels
// TODO: DM channels
// TODO: stage channels
// TODO: kanban channels
// TODO: calendar channels
@Entity('channels')
export class Channel extends CustomBaseEntity {
  /*
		VOICE
	*/

  @Column({ nullable: true })
  bitrate?: number;

  @Column({ nullable: true })
  user_limit?: number;

  /*
		TEXT
	*/

  @Column({ nullable: true })
  last_message_id: string;

  @Column({ nullable: true })
  last_pin_timestamp?: number;

  @Column({ nullable: true })
  rate_limit?: number;

  /*
		GUILD
	*/

  @Column({ nullable: true })
  @RelationId((channel: Channel) => channel.guild)
  guild_id?: string;

  @JoinColumn({ name: 'guild_id' })
  @ManyToOne(() => Guild, {
    onDelete: 'CASCADE',
  })
  guild: Guild;

  @Column({ nullable: true })
  @RelationId((channel: Channel) => channel.parent)
  parent_id: string;

  @JoinColumn({ name: 'parent_id' })
  @ManyToOne(() => Channel)
  parent?: Channel;

  @Column({ nullable: true })
  position?: number;

  @Column({ nullable: true })
  description?: string;

  @Column({ type: 'simple-json', nullable: true })
  permission_overwrites?: ChannelPermissionOverwrite[];

  @Column({ nullable: true })
  nsfw?: boolean;

  /*
		GENERAL
	*/
  @Column()
  created_at: Date;

  @Column({ nullable: true })
  name?: string;

  @Column({ type: 'text', nullable: true })
  icon?: string | null; // 'rules' or null

  @Column({ type: 'simple-enum', enum: ChannelType })
  type: ChannelType;

  @OneToMany(() => Message, (message: Message) => message.channel, {
    cascade: true,
    orphanedRowAction: 'delete',
  })
  messages?: Message[];

  static async deleteChannel(channel: Channel) {
    // TODO: remove message attachments etc.
    await Message.delete({ channel_id: channel.id });
    await Channel.delete({ id: channel.id });
  }

  static async createChannel(
    channel: Partial<Channel>,
    user_id = '0',
    skipEventEmit = false,
    skipExistsCheck = false,
    generateId = true,
  ) {
    // TODO: channel create permissions

    switch (channel.type) {
      case ChannelType.TEXT:
      case ChannelType.VOICE:
        if (channel.parent_id && !skipExistsCheck) {
          const exists = await Channel.findOne({ id: channel.parent_id });
          if (!exists) throw new HTTPError("Parent channel doesn't exist for " + channel.id, 400);
          if (exists.guild_id !== channel.guild_id)
            throw new HTTPError('The category channel needs to be in the same guild', 400);
        }
        break;
      case ChannelType.STAGE:
      case ChannelType.KANBAN:
      case ChannelType.CALENDAR:
      case ChannelType.CATEGORY:
        throw new HTTPError('Not yet supported');
      default:
        throw new HTTPError('Unknown channel type');
    }

    if (!channel.permission_overwrites) channel.permission_overwrites = [];
    // TODO: auto generate position

    channel = {
      ...channel,
      ...(generateId && { id: Snowflake.generate() }),
      created_at: new Date(),
      position: channel.position || 0,
    };

    await Promise.all([
      Channel.create(channel).save(),
      !skipEventEmit
        ? emitEvent({
            event: 'CHANNEL_CREATE',
            data: channel,
            guild_id: channel.guild_id,
          } as ChannelCreateEvent)
        : Promise.resolve(),
    ]);

    return channel;
  }
}

export interface ChannelPermissionOverwrite {
  allow: bigint; // for bitfields we use bigints
  deny: bigint; // for bitfields we use bigints
  id: string;
  type: ChannelPermissionOverwriteType;
}

export enum ChannelPermissionOverwriteType {
  role = 0,
  member = 1,
}
