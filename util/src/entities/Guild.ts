import { Column, Entity, ManyToOne, JoinColumn, RelationId, OneToMany } from 'typeorm';
import { CustomBaseEntity } from './CustomBaseEntity';
import { User } from './User';
import { Member } from './Member';
import { Channel } from './Channel';
import { MessageNotificationLevel } from './Member';

@Entity('guilds')
export class Guild extends CustomBaseEntity {
  @Column()
  name: string;

  @Column({ nullable: true })
  description?: string;

  @Column({ nullable: true })
  icon_url?: string;

  @Column({ nullable: true })
  banner_url?: string;

  @Column({ nullable: true })
  member_count?: number;

  @Column()
  default_message_notifications: MessageNotificationLevel;

  @Column()
  created_at: Date;

  @OneToMany(() => Member, (member: Member) => member.guild, {
    cascade: true,
    orphanedRowAction: 'delete',
    onDelete: 'CASCADE',
  })
  members: Member[];

  @JoinColumn({ name: 'channel_ids' })
  @OneToMany(() => Channel, (channel: Channel) => channel.guild, {
    cascade: true,
    orphanedRowAction: 'delete',
  })
  channels: Channel[];

  @Column({ nullable: true })
  @RelationId((guild: Guild) => guild.owner)
  owner_id: string;

  @JoinColumn({ name: 'owner_id', referencedColumnName: 'id' })
  @ManyToOne(() => User)
  owner: User;

  @Column({ nullable: true })
  boost_subscription_count?: number;

  @Column({ nullable: true })
  boost_tier?: number; // "nitro" boost level

  // TODO: custom invite url
  // TODO: invites
}

export const PublicGuildRelations = ['channels', 'members.user', 'members'];
