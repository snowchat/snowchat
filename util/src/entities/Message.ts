import { Column, Entity, ManyToOne, JoinColumn, RelationId, CreateDateColumn, OneToMany } from 'typeorm';
import { CustomBaseEntity } from './CustomBaseEntity';
import { User } from './User';
import { Channel } from './Channel';
import { Guild } from './Guild';
import { Member } from './Member';

export enum MessageType {
  DEFAULT = 0,
  CALL = 1,
  GUILD_MEMBER_JOIN = 2,
  BOOST = 3,
  BOOST_TIER_1 = 4,
  BOOST_TIER_2 = 5,
  BOOST_TIER_3 = 6,
  CHANNEL_FOLLOW_ADD = 7,
  REPLY = 8,
  EVENT_REMINDER = 9,
  VOTE = 10,
}

@Entity('messages')
export class Message extends CustomBaseEntity {
  @Column({ nullable: true })
  @RelationId((message: Message) => message.channel)
  channel_id: string;

  @JoinColumn({ name: 'channel_id' })
  @ManyToOne(() => Channel, {
    onDelete: 'CASCADE',
  })
  channel: Channel;

  @Column({ nullable: true })
  @RelationId((message: Message) => message.guild)
  guild_id?: string;

  @JoinColumn({ name: 'guild_id' })
  @ManyToOne(() => Guild, {
    onDelete: 'CASCADE',
  })
  guild?: Guild;

  @Column({ nullable: true })
  @RelationId((message: Message) => message.author)
  author_id: string;

  @JoinColumn({ name: 'author_id', referencedColumnName: 'id' })
  @ManyToOne(() => User, {
    onDelete: 'CASCADE',
  })
  author?: User;

  @Column({ nullable: true })
  @RelationId((message: Message) => message.member)
  member_id: string;

  @JoinColumn({ name: 'member_id' })
  @ManyToOne(() => Member)
  member?: Member;

  @Column({ nullable: true })
  content?: string;

  @Column()
  @CreateDateColumn()
  timestamp: Date;

  @Column({ nullable: true })
  edited_timestamp?: Date;

  @Column({ nullable: true })
  tts?: boolean;

  // TODO: attachments
  //@OneToMany(() => Attachment, (attachment: Attachment) => attachment.message, {
  //	cascade: true,
  //	orphanedRowAction: "delete",
  //})
  //attachments?: Attachment[];

  @Column({ nullable: true })
  pinned?: boolean; // TODO: create index for pinned messages in a channel

  @Column({ type: 'simple-enum', enum: MessageType })
  type: MessageType;

  @Column({ type: 'simple-json', nullable: true })
  activity?: {
    type: number;
    party_id: string;
  };

  @Column({ nullable: true })
  flags?: string;
  @Column({ type: 'simple-json', nullable: true })
  message_reference?: {
    message_id: string;
    channel_id?: string;
    guild_id?: string;
  };

  @JoinColumn({ name: 'message_reference_id' })
  @ManyToOne(() => Message)
  referenced_message?: Message;

  @Column({ type: 'simple-json', nullable: true })
  components?: MessageComponent[];
}

export enum MessageComponentType {
  button = 0,
  dropdown = 1,
}

export interface MessageComponent {
  type: MessageComponentType;
  label?: string;
  custom_id?: string;
  url?: string;
  disabled?: boolean;
  components: MessageComponent[];
}
