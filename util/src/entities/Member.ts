import { PublicUser, User } from './User';
import { Guild, PublicGuildRelations } from './Guild';
import { Column, Entity, Index, PrimaryGeneratedColumn, RelationId, JoinColumn, ManyToMany, ManyToOne } from 'typeorm';
import { CustomBaseEntityWithoutId } from './CustomBaseEntity';
import { config } from '../util/config';
import { emitEvent } from '../util/events';
import { GuildCreateEvent, GuildDeleteEvent, GuildMemberRemoveEvent } from '../interfaces/guildEvents';
import { HTTPError } from 'lambert-server';
import { GuildMemberAddEvent } from '../interfaces/guildEvents';

// TODO: public/featured guilds
@Entity('members')
@Index(['id', 'guild_id'], { unique: true })
export class Member extends CustomBaseEntityWithoutId {
  @PrimaryGeneratedColumn()
  index: string;

  @Column()
  @RelationId((member: Member) => member.user)
  id: string;

  @JoinColumn({ name: 'id' })
  @ManyToOne(() => User, {
    onDelete: 'CASCADE',
  })
  user: User;

  @Column()
  @RelationId((member: Member) => member.guild)
  guild_id: string;

  @JoinColumn({ name: 'guild_id' })
  @ManyToOne(() => Guild, {
    onDelete: 'CASCADE',
  })
  guild: Guild;

  @Column({ nullable: true })
  nick?: string;

  @Column()
  joined_at: Date;

  @Column({ type: 'simple-json' })
  settings: UserGuildSettings;

  static async addToGuild(user_id: string, guild_id: string) {
    const user = await User.getPublicUser(user_id);
    // TODO: bans
    const { maxJoinedGuilds } = config.get().limits.user;
    const userGuildCount = await Member.count({ id: user_id });
    if (userGuildCount >= maxJoinedGuilds) {
      throw new HTTPError(`Maximum number of guilds reached (${maxJoinedGuilds}).`, 400);
    }

    const guild = await Guild.findOne({
      where: {
        id: guild_id,
      },
      relations: PublicGuildRelations,
    });
    if (!guild) throw new HTTPError('Guild not found', 404);

    if (await Member.count({ id: user.id, guild: { id: guild_id } }))
      throw new HTTPError('You are already a member of this guild', 400);

    const member = {
      id: user_id,
      guild_id,
      nick: undefined,
      //roles: [guild_id], // @everyone role
      joined_at: new Date(),
      premium_since: undefined,
      deaf: false,
      mute: false,
      pending: false,
    };

    await Promise.all([
      // @ts-ignore
      Member.create(
        {
          ...member,
          // TODO: roles, context of todo: @everyone
          //roles: [new Role({ id: guild_id })],
          // read_state: {},
          settings: {
            channel_overrides: [],
            message_notifications: 0,
            mobile_push: true,
            muted: false,
            suppress_everyone: false,
            suppress_roles: false,
            version: 0,
          },
          // Member.save is needed because else the roles relations wouldn't be updated
        } as Partial<Member>
      ).save(),
      Guild.increment({ id: guild_id }, 'member_count', 1),
      emitEvent({
        event: 'GUILD_MEMBER_ADD',
        data: {
          ...member,
          user,
          guild_id,
        },
        guild_id,
      } as GuildMemberAddEvent),
      emitEvent({
        event: 'GUILD_CREATE',
        // TODO: review event code later, see https://en.wikipedia.org/wiki/Data_transfer_object
        data: {
          ...guild,
          members: [...guild.members, { ...member, user }],
          member_count: (guild.member_count || 0) + 1,
          joined_at: member.joined_at,
        } as Partial<Guild>,
        user_id,
      } as GuildCreateEvent),
    ]);
  }

  static async removeFromGuild(user_id: string, guild_id: string) {
    const guild = await Guild.findOne({ select: ['owner_id'], where: { id: guild_id } });
    if (!guild) throw new HTTPError('Guild not found', 404);
    if (guild.owner_id === user_id) throw new Error('The owner cannot be removed of the guild');
    const member = await Member.findOne({ where: { id: user_id, guild_id }, relations: ['user'] });
    if (!member) throw new HTTPError('Member not found', 404);

    // use promise all to execute all promises at the same time -> save time
    return Promise.all([
      Member.delete({
        id: user_id,
        guild_id,
      }),
      Guild.decrement({ id: guild_id }, 'member_count', -1),

      emitEvent({
        event: 'GUILD_DELETE',
        data: {
          id: guild_id,
        },
        user_id: user_id,
      } as GuildDeleteEvent),
      emitEvent({
        event: 'GUILD_MEMBER_REMOVE',
        data: { guild_id, user: member.user },
        guild_id,
      } as GuildMemberRemoveEvent),
    ]);
  }
}

export interface UserGuildSettings {
  channel_overrides: {
    channel_id: string;
    message_notifications: MessageNotificationLevel;
    muted: boolean;
  }[];
  message_notifications: MessageNotificationLevel;
  muted: boolean;
  mobile_push: boolean;
  version: number;
}

export enum MessageNotificationLevel {
  ALL = 0,
  EVERYONE_MENTIONS = 1,
  ROLE_MENTIONS = 2,
  USER_MENTIONS = 3,
  NONE = 4,
}

export type PublicMemberKeys = 'id' | 'guild_id' | 'nick' | 'joined_at';

export const PublicMemberProjection: PublicMemberKeys[] = ['id', 'guild_id', 'nick', 'joined_at'];

export type PublicMember = Pick<Member, PublicMemberKeys> & {
  user: PublicUser;
  // TODO: roles
  //roles: string[]; // Role IDs
};
