import { Column, Entity, FindOneOptions } from 'typeorm';
import { CustomBaseEntity } from './CustomBaseEntity';
import { HTTPError } from 'lambert-server';

export enum PublicUserEnum {
  id,
  username,
  discriminator,
  avatar_url,
  banner_url,
  bio,
  bot,
  developer,
  status,
}
export type PublicUserKeys = keyof typeof PublicUserEnum;

export enum PrivateUserEnum {
  email,
  disabled,
}
export type PrivateUserKeys = keyof typeof PrivateUserEnum | PublicUserKeys;

export const PublicUserProjection = Object.values(PublicUserEnum).filter(
  (x) => typeof x === 'string',
) as PublicUserKeys[];
export const PrivateUserProjection = [
  ...PublicUserProjection,
  ...Object.values(PrivateUserEnum).filter((x) => typeof x === 'string'),
] as PrivateUserKeys[];

// Private user data that should never get sent to the client
export type PublicUser = Pick<User, PublicUserKeys>;

export interface UserPublic extends Pick<User, PublicUserKeys> {}

export interface UserPrivate extends Pick<User, PrivateUserKeys> {}

// * Guilds not in folders are GuildFolder > GuildFolderItem

export interface GuildFolder {
  color?: number;
  guild_ids: string[];
  position: number;
  id: number;
  name?: string;
}

@Entity('users')
export class User extends CustomBaseEntity {
  @Column()
  password_hash: string;

  @Column()
  email: string; // length constraints: 3-254

  @Column()
  username: string; // length constraints: 3-30

  @Column()
  discriminator: number; // 0001-9999

  @Column({ nullable: true })
  avatar_url?: string;

  @Column({ nullable: true })
  banner_url?: string;

  @Column({ nullable: true })
  bio?: string; // short description of the user (max 190 chars)

  @Column()
  bot: boolean; // if the user is a bot

  @Column()
  developer: boolean; // if the user is a verified developer of Snowchat

  @Column()
  created_at: Date; // registration date

  @Column()
  status: 'online' | 'idle' | 'dnd' | 'offline';

  @Column()
  disabled: boolean;

  @Column()
  deleted: boolean;

  @Column('simple-array') // string in simple-array must not contain commas
  guilds: string[]; // array of guild ids the user is part of

  @Column('simple-array')
  guild_folders: GuildFolder[];

  @Column('simple-json')
  custom_status: {
    emoji_id?: string;
    expires_at?: Number;
    text?: string;
  };

  @Column()
  valid_tokens_since: Date;

  static async getPublicUser(user_id: string, opts?: FindOneOptions<User>) {
    const user = await User.findOne(
      { id: user_id },
      {
        ...opts,
        select: [...PublicUserProjection, ...(opts?.select || [])],
      },
    );
    if (!user) {
      console.log('[Util] getPublicUser: User not found!', user_id);
      throw new HTTPError('User not found', 404);
    }
    return user;
  }
}
