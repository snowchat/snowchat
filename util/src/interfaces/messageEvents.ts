import { Message } from '../entities/Message';
import { Event } from './event';

export interface MessageCreateEvent extends Event {
  event: 'MESSAGE_CREATE';
  data: Partial<Message>;
}

export interface MessageUpdateEvent extends Event {
  event: 'MESSAGE_UPDATE';
  data: Partial<Message>;
}

export interface MessageDeleteEvent extends Event {
  event: 'MESSAGE_DELETE';
  data: {
    id: string;
    channel_id: string;
    guild_id?: string;
  };
}

export type MessageEventData = MessageCreateEvent | MessageUpdateEvent | MessageDeleteEvent;
export type MessageEventType = 'MESSAGE_CREATE' | 'MESSAGE_UPDATE' | 'MESSAGE_DELETE';
