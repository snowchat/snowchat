import { Event } from './event';
import { Guild } from '../entities/Guild';
import { PublicMember } from '../entities/Member';
import { PublicUser } from '../entities/User';
import { Channel } from '../entities/Channel';

export interface GuildCreateEvent extends Event {
  event: 'GUILD_CREATE';
  data: Partial<Guild>;
}

export interface GuildDeleteEvent extends Event {
  event: 'GUILD_DELETE';
  data: {
    id: string;
  };
}

export interface GuildMemberRemoveEvent extends Event {
  event: 'GUILD_MEMBER_REMOVE';
  data: {
    guild_id: string;
    user: PublicUser;
  };
}

export interface GuildMemberAddEvent extends Event {
  event: 'GUILD_MEMBER_ADD';
  data: PublicMember & {
    user: PublicUser;
    guild_id: string;
  };
}

export interface ChannelCreateEvent extends Event {
  event: 'CHANNEL_CREATE';
  data: Partial<Channel>;
}

export interface ChannelUpdateEvent extends Event {
  event: 'CHANNEL_UPDATE';
  data: Partial<Channel>;
}

export interface ChannelDeleteEvent extends Event {
  event: 'CHANNEL_DELETE';
  data: Partial<Channel>;
}

export type GuildEventData =
  | GuildCreateEvent
  | GuildDeleteEvent
  | GuildMemberRemoveEvent
  | GuildMemberAddEvent
  | ChannelCreateEvent
  | ChannelDeleteEvent
  | ChannelUpdateEvent;

export type GuildEventType =
  | 'GUILD_CREATE'
  | 'GUILD_DELETE'
  | 'GUILD_MEMBER_REMOVE'
  | 'GUILD_MEMBER_ADD'
  | 'CHANNEL_CREATE'
  | 'CHANNEL_DELETE'
  | 'CHANNEL_UPDATE';
