import { PublicUser } from '../entities/User';
import { GuildEventData, GuildEventType } from './guildEvents';
import { MessageEventData, MessageEventType } from './messageEvents';

// Most events have their data (Entities, etc.) wrapped
// with a Partial<> so there is no requirement to send
// for example the helper functions for entities

export interface Event {
  guild_id?: string;
  user_id?: string;
  channel_id?: string;
  created_at?: Date;
  event: EventType;
  data?: any;
}

export type EventType = MessageEventType | GuildEventType | 'USER_UPDATE';

export type EventData = MessageEventData | GuildEventData | UserUpdateEvent;

//#region Events

export interface UserUpdateEvent extends Event {
  event: 'USER_UPDATE';
  data: PublicUser;
}

//#endregion Events
