import http from 'http';
import { APIServer, ApiServerOptions } from '@snowchat/http-api';
import { GatewayServer } from '@snowchat/gateway';
import { config } from '@snowchat/util';
import express from 'express';

const app = express();
const server = http.createServer();
const port = Number(process.env.PORT) || 4000;
const production = false;
server.on('request', app);

export async function startServers() {
  const apiServerOptions: Partial<ApiServerOptions> = {
    server,
    port,
    production,
    app,
  };
  const api = new APIServer(apiServerOptions);
  const gateway = new GatewayServer({ server, port, production });

  await Promise.all([api.start(), gateway.start()]);
  console.log(`[Server] listening on port ${port}`);
}
