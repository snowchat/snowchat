#!/usr/bin/env node
process.on('uncaughtException', console.error);
process.on('unhandledRejection', console.error);

import cluster from 'cluster';
import os from 'os';
import { initStats } from './stats';
import { startServers } from './index';

if (
  cluster.isPrimary &&
  process.env.NODE_ENV == 'production' &&
  process.env.SNOWCHAT_THREADED?.toLowerCase() == 'true'
) {
  initStats();

  const cores = 1 || Number(process.env.threads) || os.cpus().length;
  // Fork workers.
  for (let i = 0; i < cores; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker: any, code: any, signal: any) => {
    console.log(`[Worker] died with pid: ${worker.process.pid} , restarting ...`);
    cluster.fork();
  });
} else {
  startServers().catch(console.error);
}
